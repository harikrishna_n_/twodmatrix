﻿using icDMDecNetPro;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace _2DMatrixCodeDecoder
{
    public class CodeDecoder
    {
        [DllImport("DM_EP_64.dll", EntryPoint = "Connect_DM_Decoder", CallingConvention = CallingConvention.StdCall)]
        internal static extern IntPtr Connect_DM_Decoder([In] Int32 MaxColCount, [In] Int32 MaxRowCount);

        private Image SourcePicture = null;
        private Bitmap Picture;
        public icDataMatrix_Decoder_Pro Decoder;
        public 
        
        int DMCount;

        public CodeDecoder()
        {
            try
            {
                IntPtr decPtr = Connect_DM_Decoder(8192, 8192); //8192
                Decoder = new icDataMatrix_Decoder_Pro(decPtr);
                Decoder.MaxDMCount = 100;
                Decoder.MirrorMode = DM_MIRROR_MODE.MM_ANY;
                Decoder.SpeedMode = DM_SPEED.DMSP_EXPRESS.GetHashCode();
                Decoder.LabelMode = DM_LABEL_MODE.LM_ST_DOT;
                Decoder.CellColor = DM_CELL_COLOR.CL_ANY;
                Decoder.QzMode = DMQZ_MODE.DMQZ_SMALL.GetHashCode();
                Decoder.FilterMode = DM_FILTER_MODE.FM_SHARPMASK;

                
            }
            catch (Exception ex)
            {
            }
        }


        public Image ProcessImageAndDecode(Image image, ref List<string> codes, ref List<DecodedPoint> matchedPoints)
        {
            try
            {
                SourcePicture = image;
                CopyPicture();
              //  Decoder.FormToMode();
                Decoder.DecodeImage(Picture);
                DMCount = Decoder.MaxDMCount;

                for (var i = 0; i < DMCount; i++)
                {
                    TDM_Info iInfo = Decoder.Get_DM_Info(i);
                    TRowCols RC = iInfo.RowCols;
                    DrawCorners(true, RC, ref matchedPoints);

                    TDM_Info FDM_Info = Decoder.Get_DM_Info(i);
                    if (FDM_Info != null)
                    {
                        codes.Add(FDM_Info.GetDecodedChars(1252));
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Picture;
        }

        private void CopyPicture()
        {
            Bitmap B = new Bitmap(SourcePicture);
            Rectangle Re = new Rectangle(0, 0, B.Width, B.Height);
            Picture = B.Clone(Re, PixelFormat.Format24bppRgb);
        }

        private void DrawCorners(bool Ok, TRowCols RowCols, ref List<DecodedPoint> matchedPoints)
        {
            if (Picture == null) return;
            float X1, X2, Y1, Y2;
            int I1, I2, I;
            Pen P;
            float wid = Picture.Width / 600 + 3;
            if (Ok)
                P = new Pen(Color.Lime, wid);
            else
                P = new Pen(Color.Red, wid);
            Graphics G = Graphics.FromImage(Picture);
            for (I = 0; I < 4; I++)
            {
                I1 = (I * 2 + 0);
                I2 = (I * 2 + 2);
                if (I2 > 7) I2 = 0;
                X1 = (float)RowCols[I1 + 1];
                Y1 = (float)RowCols[I1 + 0];
                X2 = (float)RowCols[I2 + 1];
                Y2 = (float)RowCols[I2 + 0];
                if (I == 0)
                    matchedPoints.Add(new DecodedPoint() { X1 = X1, Y1 = Y1, X2 = X2, Y2 = Y2 });
                G.DrawLine(P, X1, Y1, X2, Y2);
            }
        }
    }
}
