﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Json.Net.Entities
{
    public class MatrixProgram
    {
        public int ProgramID { get; set; }
        public string Name { get; set; }
        public int Matrix_X { get; set; }
        public int Matrix_Y { get; set; }
    }
}
