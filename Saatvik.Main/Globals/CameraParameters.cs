﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PylonC.NET;
using PylonC.NETSupportLibrary;

namespace Saatvik.Globals
{
    public class CameraParameters
    {
        public string TestImagefeatureval;
        public string PixalFormatfeatureval;
        public string ShutterModefeatureval;
        public long ExposureTimeRawfeatureval;
        public long GainRawfeatureval;
        public long Heightfeatureval;
        public long Widthfeatureval;
        public long InterPacketDelayfeatureval;
        public long PacketSizefeatureval;
        public ImageProvider m_imageProvider = null;
        public NODE_HANDLE m_hNode = new NODE_HANDLE();
        public string programName;


        public CameraParameters(ImageProvider imgProvider)
        {

            m_imageProvider = imgProvider;


        }
        public void setProgramName(string programName)
        {
            this.programName = programName;
        }
        public void FetchCameraParameters()
        {

            string[] strnodes = { "TestImageSelector", "PixelFormat", "ShutterMode", "ExposureTimeRaw",
                                  "GainRaw", "Height", "Width" , "InterPacketDelay" , "PacketSize"};

            foreach (String name in strnodes)
            {

                m_hNode = m_imageProvider.GetNodeFromDevice(name);
                if (!m_hNode.IsValid /* No node has been found using the provided name. */
                    && (name == "GainRaw" || name == "ExposureTimeRaw" || name == "PacketSize" || name == "InterPacketDelay")) /* This means probably that the camera is compliant to a later SFNC version. */
                {
                    /* Check to see if a compatible node exists. The SFNC 2.0, implemented by Basler USB Cameras for instance, defines Gain
                       and ExposureTime as features of type Float.*/
                    if (name == "GainRaw")
                    {
                        m_hNode = m_imageProvider.GetNodeFromDevice("Gain");
                        m_hNode = GenApi.NodeGetAlias(m_hNode);
                        GainRawfeatureval = checked((int)GenApi.IntegerGetValue(m_hNode));
                    }
                    else if (name == "ExposureTimeRaw")
                    {
                        m_hNode = m_imageProvider.GetNodeFromDevice("ExposureTime");
                        m_hNode = GenApi.NodeGetAlias(m_hNode);
                        ExposureTimeRawfeatureval = checked((int)GenApi.IntegerGetValue(m_hNode));
                    }
                    else if (name == "PacketSize")
                    {
                        m_hNode = m_imageProvider.GetNodeFromDevice("GevSCPSPacketSize");
                        PacketSizefeatureval = checked((int)GenApi.IntegerGetValue(m_hNode));
                    }

                    else if (name == "InterPacketDelay")
                    {
                        m_hNode = m_imageProvider.GetNodeFromDevice("GevSCPD");
                        InterPacketDelayfeatureval = checked((int)GenApi.IntegerGetValue(m_hNode));
                    }


                }
                else if (m_hNode.IsValid)
                {

                    if (name == "ExposureTimeRaw")
                    {
                        ExposureTimeRawfeatureval = checked((int)GenApi.IntegerGetValue(m_hNode));
                    }

                    if (name == "GainRaw")
                    {
                        GainRawfeatureval = checked((int)GenApi.IntegerGetValue(m_hNode));
                    }
                    if (name == "TestImageSelector")
                    {
                        TestImagefeatureval = GenApi.NodeToString(m_hNode);
                    }
                    else if (name == "PixelFormat")
                    {
                        PixalFormatfeatureval = GenApi.NodeToString(m_hNode);
                    }
                    else if (name == "ShutterMode")
                    {
                        ShutterModefeatureval = GenApi.NodeToString(m_hNode);
                    }
                    else if (name == "Height")
                    {
                        Heightfeatureval = checked((int)GenApi.IntegerGetValue(m_hNode));
                    }
                    else if (name == "Width")
                    {
                        Widthfeatureval = checked((int)GenApi.IntegerGetValue(m_hNode));
                    }

                }


            }
            
        }


        public void SetCameraParameters()
        {
            string[] strnodes = { "TestImageSelector", "PixelFormat", "ShutterMode", "ExposureTimeRaw",
                                  "GainRaw", "Height", "Width" , "InterPacketDelay" , "PacketSize"};

            foreach (String name in strnodes)
            {
                m_hNode = m_imageProvider.GetNodeFromDevice(name);

                if (name == "TestImageSelector")
                {
                    GenApi.NodeFromString(m_hNode, TestImagefeatureval);

                }
                else if (name == "PixelFormat")
                {
                    GenApi.NodeFromString(m_hNode, PixalFormatfeatureval);

                }
                else if (name == "ShutterMode")
                {
                    GenApi.NodeFromString(m_hNode, ShutterModefeatureval);
                                  
                }
                else if (name == "ExposureTimeRaw")
                {
                    GenApi.IntegerSetValue(m_hNode, ExposureTimeRawfeatureval);
                   
                }
                else if (name == "GainRaw")
                {
                    GenApi.IntegerSetValue(m_hNode, GainRawfeatureval);
                }
                else if (name == "Height")
                {
                    GenApi.IntegerSetValue(m_hNode, Heightfeatureval);
                }
                else if (name == "Width")
                {
                    GenApi.IntegerSetValue(m_hNode, Widthfeatureval);
                }
                else if (name == "InterPacketDelay")
                {
                    m_hNode = m_imageProvider.GetNodeFromDevice("GevSCPD");
                    GenApi.IntegerSetValue(m_hNode, InterPacketDelayfeatureval);

                }
                else if (name == "PacketSize")
                {
                    m_hNode = m_imageProvider.GetNodeFromDevice("GevSCPSPacketSize");
                    GenApi.IntegerSetValue(m_hNode, PacketSizefeatureval);
                
                }
             
            }

       }

        public void SetDefaultValues()
        {

             string[] strnodes = { "TestImageSelector", "PixelFormat", "ShutterMode", "ExposureTimeRaw",
                                  "GainRaw", "Height", "Width" , "InterPacketDelay" , "PacketSize"};
             long min;
             long max; 

             foreach (String name in strnodes)
             {
                 m_hNode = m_imageProvider.GetNodeFromDevice(name);

                 if (name == "TestImageSelector")
                 {
                     GenApi.NodeFromString(m_hNode,"Off");

                 }
                 else if (name == "PixelFormat")
                 {
                     GenApi.NodeFromString(m_hNode, "Mono8");

                 }
                 else if (name == "ShutterMode")
                 {
                     GenApi.NodeFromString(m_hNode,"Rolling");

                 }
               
                 else  if (name == "ExposureTimeRaw")
                 {
                   //  min = GenApi.IntegerGetMin(m_hNode);
                   //GenApi.IntegerSetValue(m_hNode, min);
                     GenApi.IntegerSetValue(m_hNode,35000);

                 }
                 else if (name == "GainRaw")
                 {
                     min = GenApi.IntegerGetMin(m_hNode);
                     GenApi.IntegerSetValue(m_hNode, min);
                 }
                 else if (name == "Height")
                 {
                     max = GenApi.IntegerGetMax(m_hNode);
                     GenApi.IntegerSetValue(m_hNode, max);
                 }
                 else if (name == "Width")
                 {
                     max = GenApi.IntegerGetMax(m_hNode);
                     GenApi.IntegerSetValue(m_hNode, max);
                 }
                 else if (name == "InterPacketDelay")
                 {
                     m_hNode = m_imageProvider.GetNodeFromDevice("GevSCPD");
                     GenApi.IntegerSetValue(m_hNode, 1500);

                 }
                 else if (name == "PacketSize")
                 {
                     m_hNode = m_imageProvider.GetNodeFromDevice("GevSCPSPacketSize");
                     GenApi.IntegerSetValue(m_hNode, 1500);

                 }
             }

        }

    }
}

