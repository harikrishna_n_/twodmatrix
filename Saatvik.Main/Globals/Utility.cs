﻿using Saatvik.Entities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Saatvik.Globals
{
    public class Utility
    {
        public enum AccessLevelTypes
        {
            Operator = 0,
            Admin = 1
        }
        public static AccessLevelTypes AccessLevelType { get; set; }
        public static bool IsOnlineModeEnabled = false;
        public static bool IsBusy = false;
        private static readonly Object obj = new Object();
        public static VisionProgram CurrentProgram = new VisionProgram();
        private static string programsFolderPath = AppDomain.CurrentDomain.BaseDirectory + "Programs";
        public static string codeResultsFolderPath = string.Empty;
        
        static Utility()
        {
            try
            {
                if (!Directory.Exists(programsFolderPath))
                {

                  Directory.CreateDirectory(programsFolderPath);
                }
            }
            catch 
            {
             }
        }

        public static bool CreateProgram(VisionProgram program)
        {
            try
            {
                var path = programsFolderPath + "\\" + program.Name + ".sat";
                using (var file = File.Create(path)) { };

                using (StreamWriter sw = new StreamWriter(path))
                {
                    sw.Write(program.Name + "|" + program.Rows + "|" + program.Columns + "|" + program.Region.Location.X +
                        "|" + program.Region.Location.Y + "|" + program.Region.Width + "|" + program.Region.Height + "|" + 
                        program.RotationAngle + "|"+ program.MaxDMCount + "|" + program.Mirror + "|" + program.DecodeMode + 
                        "|" + program.LabelMode + "|" + program.CalcQuality + "|" + program.Color + "|" + program.Filter + 
                        "|" + program.Quietzone + "|" + program.Timeout);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static List<string> GetAvailablePrograms()
        {
            var programs = new List<string>();
            try
            {
                foreach (var file in Directory.EnumerateFiles(programsFolderPath, "*.sat"))
                {
                    var fileName = file.Replace(programsFolderPath + "\\", "").Replace(".sat", "");
                    programs.Add(fileName);
                }
            }
            catch
            {

            }
            return programs;
        }

        public static void LoadProgram(string selectedProgram)
        {
            var path = programsFolderPath + "\\" + selectedProgram;
          // CurrentProgram = new VisionProgram();
            try
            {
                if (File.Exists(path))
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        var textLine = sr.ReadLine();
                        var data = textLine.Split('|');
                        CurrentProgram.Name = data[0];
                        CurrentProgram.Rows = Convert.ToInt32(data[1]);
                        CurrentProgram.Columns = Convert.ToInt32(data[2]);
                        CurrentProgram.Region = new Rectangle(Convert.ToInt32(data[3]), Convert.ToInt32(data[4]), Convert.ToInt32(data[5]), Convert.ToInt32(data[6]));
                        CurrentProgram.RotationAngle = Convert.ToInt32(data[7]);
                        CurrentProgram.MaxDMCount = Convert.ToInt32(data[8]);
                        CurrentProgram.Mirror = Convert.ToInt32(data[9]);
                        CurrentProgram.DecodeMode = Convert.ToInt32(data[10]);
                        CurrentProgram.LabelMode = Convert.ToInt32(data[11]);
                        CurrentProgram.CalcQuality = Convert.ToInt32(data[12]);
                        CurrentProgram.Color = Convert.ToInt32(data[13]);
                        CurrentProgram.Filter = Convert.ToInt32(data[14]);
                        CurrentProgram.Quietzone = Convert.ToInt32(data[15]);
                        CurrentProgram.Timeout = Convert.ToInt32(data[16]);

                    }
                }
            }
            catch
            {
            }
        }

        public static VisionProgram FetchProgram(string programName)
        {
            var path = programsFolderPath + "\\" + programName + ".sat";
           
           var program = new VisionProgram();
            try
            {
                if (File.Exists(path))
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        var textLine = sr.ReadLine();
                        var data = textLine.Split('|');
                        program.Name = data[0];
                        program.Rows = Convert.ToInt32(data[1]);
                        program.Columns = Convert.ToInt32(data[2]);
                        program.Region = new Rectangle(Convert.ToInt32(data[3]), Convert.ToInt32(data[4]), Convert.ToInt32(data[5]), Convert.ToInt32(data[6]));
                        CurrentProgram.RotationAngle = Convert.ToInt32(data[7]);
                        CurrentProgram.MaxDMCount = Convert.ToInt32(data[8]);
                        CurrentProgram.Mirror = Convert.ToInt32(data[9]);
                        CurrentProgram.DecodeMode = Convert.ToInt32(data[10]);
                        CurrentProgram.LabelMode = Convert.ToInt32(data[11]);
                        CurrentProgram.CalcQuality = Convert.ToInt32(data[12]);
                        CurrentProgram.Color = Convert.ToInt32(data[13]);
                        CurrentProgram.Filter = Convert.ToInt32(data[14]);
                        CurrentProgram.Quietzone = Convert.ToInt32(data[15]);
                        CurrentProgram.Timeout = Convert.ToInt32(data[16]);

                    }
              }
         }
            catch
            {
            }
            return program;
        }

        public static void DeleteProgram(string programName)
        {
            var path = programsFolderPath + "\\" + programName + ".sat";
            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
            catch
            {
            }
        }


        public static void DeleteProgram(CameraParameters cparams)
        {
            var path = programsFolderPath + "\\" + "cameraparams" + cparams.programName + ".txt";
            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
            catch
            {
            }
        }

        public static bool CreateProgram (CameraParameters cparams)
        {
            try
            {
                var path = programsFolderPath + "\\"  + "cameraparams"  + cparams.programName + ".txt";
                using (var file = File.Create(path)) { };

                using (StreamWriter sw = new StreamWriter(path))
                {
                  sw.Write(cparams.TestImagefeatureval + "|" + cparams.PixalFormatfeatureval + "|" + cparams.ShutterModefeatureval + "|" + cparams.ExposureTimeRawfeatureval + "|" + cparams.GainRawfeatureval + "|" +
                    cparams.Heightfeatureval + "|" + cparams.Widthfeatureval + "|" +cparams.InterPacketDelayfeatureval + "|" + cparams.PacketSizefeatureval + "|");
                         
                }
               
                return true;
            }
            catch
            {
                return false;
            }
        }


        public static bool LoadProgram(CameraParameters cparams)
        {
            try
            {
                var path = programsFolderPath + "\\" + "cameraparams" + cparams.programName + ".txt";
                //using (var file = File.Create(path)) { };
                if (File.Exists(path))
                {

                    using (StreamReader sr = new StreamReader(path))
                    {
                        var textLine = sr.ReadLine();
                        var data = textLine.Split('|');

                        cparams.TestImagefeatureval = data[0];
                        cparams.PixalFormatfeatureval = data[1];
                        cparams.ShutterModefeatureval = data[2];
                        cparams.ExposureTimeRawfeatureval = Convert.ToInt64(data[3]);
                        cparams.GainRawfeatureval = Convert.ToInt64(data[4]);
                        cparams.Heightfeatureval = Convert.ToInt64(data[5]);
                        cparams.Widthfeatureval = Convert.ToInt64(data[6]);
                        cparams.InterPacketDelayfeatureval = Convert.ToInt64(data[7]);
                        cparams.PacketSizefeatureval = Convert.ToInt64(data[8]);
              
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
  

    }
}
