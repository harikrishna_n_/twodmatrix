﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saatvik.Globals
{
    public static class AppOneExtensions
    {
        public static int ToInt(this string s)
        {
            int val = 0;
            Int32.TryParse(s, out val);
            return val;
        }
    }
}
