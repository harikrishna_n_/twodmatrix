﻿using _2DMatrixCodeDecoder;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saatvik.Globals
{
    public class EmguUtility
    {
        internal static Bitmap DrawRectangle(Image<Bgr, byte> imageMat, Point location, int width, int height, DecodedPoint point = null)
        {
            try
            {
                var rectangle = new Rectangle(location.X, location.Y, width, height);
                imageMat.Draw(rectangle, new Bgr(125, 244, 66), 1);
                imageMat.Draw(new CircleF(new PointF(rectangle.Location.X + rectangle.Width / 2,
                    rectangle.Location.Y + rectangle.Height / 2),
                    rectangle.Width / 4),
                    point != null ? new Bgr(0, 255, 0) : new Bgr(0, 0, 255), 3);
                return imageMat.Bitmap;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
