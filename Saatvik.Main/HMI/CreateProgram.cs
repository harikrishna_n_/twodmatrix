﻿using Emgu.CV;
using Emgu.CV.Structure;
using PylonC.NETSupportLibrary;
using Saatvik.Entities;
using Saatvik.Globals;
using Saatvik.TwoDMatrix;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using _2DMatrixCodeDecoder;

namespace Saatvik
{   
    public partial class CreateProgram : Form
    {
     
        private ImageProvider m_imageProvider = new ImageProvider();
        private CameraParameters c_cameraparameters = null;
        private Point startLocation = new Point();
        private Image<Bgr, Byte> srcImage = null;
        private int sizeOffset = 25;
        private MainForm m_mainForm;
        private int m_rotationangle = 0;
        
        
        
        public CreateProgram(MainForm mainForm)
        {
            InitializeComponent();
            c_cameraparameters = new CameraParameters(m_imageProvider);
            m_mainForm = mainForm;
            m_imageProvider.GrabErrorEvent += new ImageProvider.GrabErrorEventHandler(OnGrabErrorEventCallback);
            m_imageProvider.DeviceRemovedEvent += new ImageProvider.DeviceRemovedEventHandler(OnDeviceRemovedEventCallback);
            m_imageProvider.DeviceOpenedEvent += new ImageProvider.DeviceOpenedEventHandler(OnDeviceOpenedEventCallback);
            m_imageProvider.DeviceClosedEvent += new ImageProvider.DeviceClosedEventHandler(OnDeviceClosedEventCallback);
            m_imageProvider.GrabbingStartedEvent += new ImageProvider.GrabbingStartedEventHandler(OnGrabbingStartedEventCallback);
            m_imageProvider.ImageReadyEvent += new ImageProvider.ImageReadyEventHandler(OnImageReadyEventCallback);
            m_imageProvider.GrabbingStoppedEvent += new ImageProvider.GrabbingStoppedEventHandler(OnGrabbingStoppedEventCallback);

            /* Provide the controls in the lower left area with the image provider object. */
            sliderGain.MyImageProvider = m_imageProvider;
            sliderExposureTime.MyImageProvider = m_imageProvider;
            sliderHeight.MyImageProvider = m_imageProvider;
            sliderWidth.MyImageProvider = m_imageProvider;
            sliderPacketSize.MyImageProvider = m_imageProvider;
            sliderInterPacketDelay.MyImageProvider = m_imageProvider;
            comboBoxTestImage.MyImageProvider = m_imageProvider;
            comboBoxPixelFormat.MyImageProvider = m_imageProvider;
            comboBoxShutterMode.MyImageProvider = m_imageProvider;
        
            /* Update the list of available devices in the upper left area. */
            UpdateDeviceList();

            //Open Camera
            if (PylonC.NET.Pylon.EnumerateDevices() != 0)
                m_imageProvider.Open(0);
                       
            /* Disable the toolStripButtons upon open of the CameraSettings form. */
            EnableButtons(false, false);
            this.buttonSaveProgram.Enabled = false;

            /*Set the camera Parameters to default Values.*/
            c_cameraparameters.SetDefaultValues();
            MaxDMCountEdit.Value = 100;
            MirrorComboBox.SelectedIndex = 0;
            SpeedComboBox.SelectedIndex = 1;
            LabelComboBox.SelectedIndex =3;
            QualityComboBox.SelectedIndex = 0;
            CellColorComboBox.SelectedIndex = 2;
            FilterComboBox.SelectedIndex= 6;
            QzComboBox.SelectedIndex = 1;
            TimeOutEdit.Value = 0;


        }
      

        #region Image Grab Methods
        private void OnGrabErrorEventCallback(Exception grabException, string additionalErrorMessage)
        {
            if (InvokeRequired)
            {
                /* If called from a different thread, we must use the Invoke method to marshal the call to the proper thread. */
                BeginInvoke(new ImageProvider.GrabErrorEventHandler(OnGrabErrorEventCallback), grabException, additionalErrorMessage);
                return;
            }
            ShowException(grabException, additionalErrorMessage);
        }

        private void OnDeviceRemovedEventCallback()
        {
            if (InvokeRequired)
            {
                /* If called from a different thread, we must use the Invoke method to marshal the call to the proper thread. */
                BeginInvoke(new ImageProvider.DeviceRemovedEventHandler(OnDeviceRemovedEventCallback));
                return;
            }
            /* Stops the grabbing of images. */
            Stop();
            /* Close the image provider. */
            CloseTheImageProvider();
        }

        private void OnDeviceOpenedEventCallback()
        {
            if (InvokeRequired)
            {
                /* If called from a different thread, we must use the Invoke method to marshal the call to the proper thread. */
                BeginInvoke(new ImageProvider.DeviceOpenedEventHandler(OnDeviceOpenedEventCallback));
                return;
            }
            /* The image provider is ready to grab. Enable the grab buttons. */
            EnableButtons(true, false);
        }

        private void OnDeviceClosedEventCallback()
        {
            if (InvokeRequired)
            {
                /* If called from a different thread, we must use the Invoke method to marshal the call to the proper thread. */
                BeginInvoke(new ImageProvider.DeviceClosedEventHandler(OnDeviceClosedEventCallback));
                return;
            }
            /* The image provider is closed. Disable all buttons. */
            /* The image provider is closed. Disable all buttons. */
            EnableButtons(false, false);
        }

        private void OnGrabbingStartedEventCallback()
        {
            if (InvokeRequired)
            {
                /* If called from a different thread, we must use the Invoke method to marshal the call to the proper thread. */
                BeginInvoke(new ImageProvider.GrabbingStartedEventHandler(OnGrabbingStartedEventCallback));
                return;
            }

            /* The image provider is grabbing. Disable the grab buttons. Enable the stop button. */
            /* Do not update device list while grabbing to avoid jitter because the GUI-Thread is blocked for a short time when enumerating. */
            updateDeviceListTimer.Stop();

            /* The image provider is grabbing. Disable the grab buttons. Enable the stop button. */
            EnableButtons(false, true);
        }

        private void OnImageReadyEventCallback()
        {
            if (InvokeRequired)
            {
                /* If called from a different thread, we must use the Invoke method to marshal the call to the proper thread. */
                BeginInvoke(new ImageProvider.ImageReadyEventHandler(OnImageReadyEventCallback));
                return;
            }

            try
            {
                /* Acquire the image from the image provider. Only show the latest image. The camera may acquire images faster than images can be displayed*/
                ImageProvider.Image image = m_imageProvider.GetLatestImage();
               
                /* Check if the image has been removed in the meantime. */
                if (image != null)
                {
                    Bitmap m_bitmap = null; /* The bitmap is used for displaying the image. */
                    /* Check if the image is compatible with the currently used bitmap. */
                    if (BitmapFactory.IsCompatible(m_bitmap, image.Width, image.Height, image.Color))
                    {
                        /* Update the bitmap with the image data. */
                        BitmapFactory.UpdateBitmap(m_bitmap, image.Buffer, image.Width, image.Height, image.Color);
                    }
                    else /* A new bitmap is required. */
                    {
                        BitmapFactory.CreateBitmap(out m_bitmap, image.Width, image.Height, image.Color);
                        BitmapFactory.UpdateBitmap(m_bitmap, image.Buffer, image.Width, image.Height, image.Color);
                    }

                    pictureBoxConfiguration.Image = m_bitmap;
                   
                    srcImage = new Image<Bgr, byte>(m_bitmap);
                }
                /* The processing of the image is done. Release the image buffer. */
                m_imageProvider.ReleaseImage();
                /* The buffer can be used for the next image grabs. */

                if (Utility.IsOnlineModeEnabled)
                {
                    m_imageProvider.OneShot();
                }
            }
            catch (Exception e)
            {
                ShowException(e, m_imageProvider.GetLastErrorMessage());
            }
        }

        private void OnGrabbingStoppedEventCallback()
        {
            if (InvokeRequired)
            {
                /* If called from a different thread, we must use the Invoke method to marshal the call to the proper thread. */
                BeginInvoke(new ImageProvider.GrabbingStoppedEventHandler(OnGrabbingStoppedEventCallback));
                return;
            }

            /* The image provider stopped grabbing. Enable the grab buttons. Disable the stop button. */
            /* Enable device list update again */
            updateDeviceListTimer.Start();

            /* The image provider stopped grabbing. Enable the grab buttons. Disable the stop button. */
            EnableButtons(m_imageProvider.IsOpen, false);
        }

        private void Stop()
        {
            /* Stop the grabbing. */
            try
            {
                Utility.IsOnlineModeEnabled = false;
                m_imageProvider.Stop();
            }
            catch (Exception e)
            {
                ShowException(e, m_imageProvider.GetLastErrorMessage());
            }
        }

        private void CloseTheImageProvider()
        {
            /* Close the image provider. */
            try
            {
                m_imageProvider.Close();
            }
            catch (Exception e)
            {
                ShowException(e, m_imageProvider.GetLastErrorMessage());
            }
        }
        #endregion

        #region Form Events
        private void buttonSaveProgram_Click(object sender, EventArgs e)
        {
            if (checkBoxNewProgram.Checked)
            {
                var isError = false;
                if (textBoxName.Text == string.Empty)
                {
                    ErpProgram.SetError(textBoxName, "Enter program name");
                    isError = true;
                }

                if (textBoxColumns.Text == string.Empty)
                {
                    ErpProgram.SetError(textBoxColumns, "Enter columns");
                    isError = true;
                }

                if (textBoxRows.Text == string.Empty)
                {
                    ErpProgram.SetError(textBoxRows, "Enter rows");
                    isError = true;
                }

                if (!isError)
                {
                    Utility.CreateProgram(new VisionProgram()
                    {
                        Name = textBoxName.Text,
                        Rows = Convert.ToInt32(textBoxRows.Text),
                        Columns = Convert.ToInt32(textBoxColumns.Text),
                        Region = new Rectangle(startLocation.X, startLocation.Y, textBoxWidth.Text.ToInt(), textBoxHeight.Text.ToInt()),
                        RotationAngle = this.m_rotationangle,
                        MaxDMCount = this.MaxDMCountEdit.Value,
                        Mirror = this.MirrorComboBox.SelectedIndex,
                        DecodeMode = this.SpeedComboBox.SelectedIndex,
                        LabelMode = this.LabelComboBox.SelectedIndex,
                        CalcQuality = this.QualityComboBox.SelectedIndex,
                        Color = this.CellColorComboBox.SelectedIndex,
                        Filter = this.FilterComboBox.SelectedIndex,
                        Quietzone = this.QzComboBox.SelectedIndex,
                        Timeout = this.TimeOutEdit.Value
                       
                    });
                    m_mainForm.LoadPgmToMainForm(textBoxName.Text);
                    
                }
            }
            else
            {
                var updatedProgram = new VisionProgram()
                {
                    Name = textBoxName.Text,
                    Rows = Convert.ToInt32(textBoxRows.Text),
                    Columns = Convert.ToInt32(textBoxColumns.Text),
                    Region = new Rectangle(startLocation.X, startLocation.Y, textBoxWidth.Text.ToInt(), textBoxHeight.Text.ToInt()),
                    RotationAngle = this.m_rotationangle,
                    MaxDMCount = this.MaxDMCountEdit.Value,
                    Mirror = this.MirrorComboBox.SelectedIndex,
                    DecodeMode = this.SpeedComboBox.SelectedIndex,
                    LabelMode = this.LabelComboBox.SelectedIndex,
                    CalcQuality = this.QualityComboBox.SelectedIndex,
                    Color = this.CellColorComboBox.SelectedIndex,
                    Filter = this.FilterComboBox.SelectedIndex,
                    Quietzone = this.QzComboBox.SelectedIndex,
                    Timeout = this.TimeOutEdit.Value
                           
                };
               
                Utility.DeleteProgram(comboBoxPrograms.SelectedItem.ToString());
                Utility.CreateProgram(updatedProgram);
                m_mainForm.LoadPgmToMainForm(textBoxName.Text);
            }
            c_cameraparameters.setProgramName(textBoxName.Text);
            c_cameraparameters.FetchCameraParameters();
            Utility.CreateProgram(c_cameraparameters);

            MessageBox.Show("Changes are saved successfully");
            this.Close();
       }

        private void buttonCaptureImage_Click(object sender, EventArgs e)
        {
            try
            {
                m_imageProvider.OneShot();
                m_rotationangle = 0;
            }
            catch (Exception ex)
            {
                ShowException(ex, m_imageProvider.GetLastErrorMessage());
            }
        }
        private void CreateProgram_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Stop grabbing images
            Stop();
            /* Close the image provider. */
            CloseTheImageProvider();
            
        }

        private void btnCreateMatrix_Click(object sender, EventArgs e)
        {
            if (srcImage == null)
                return;
            
            pictureBoxConfiguration.Image = srcImage.Bitmap;
            startLocation = new Point(textBoxLocationX.Text.ToInt(), textBoxLocationY.Text.ToInt());
            CreateMatrix(sender);
        }

       
        private void textBoxRows_TextChanged(object sender, EventArgs e)
        {
            this.buttonSaveProgram.Enabled = true;
        }

        private void CreateMatrix(object sender)
        {
            Button srcButton = (Button)sender;
           try
            {
                srcButton.Enabled = false;
                int rows, columns = 0;
                Int32.TryParse(textBoxRows.Text, out rows);
                Int32.TryParse(textBoxColumns.Text, out columns);

                if (rows == 0 || columns == 0 || srcImage == null)
                {
                    return;
                }

                var width = textBoxWidth.Text.ToInt() / columns;
                var height = textBoxHeight.Text.ToInt() / rows;
                startLocation.X = textBoxLocationX.Text.ToInt();
                startLocation.Y = textBoxLocationY.Text.ToInt();
                
               Image<Bgr, byte> imageMat = new Image<Bgr, byte>(new Bitmap(pictureBoxConfiguration.Image));
                for (int i = 0; i < rows; i++)
                {
                    for (int k = 0; k < columns; k++)
                    {
                        pictureBoxConfiguration.Image = EmguUtility.DrawRectangle(imageMat, new Point(startLocation.X + k * width, startLocation.Y + i * height), width, height);
                    }
                }
                srcButton.Enabled = true;
            }
            catch (Exception ex)
            {
                srcButton.Enabled = true;
            }
            finally
            {
            }
        }

        private void buttonMoveUp_Click(object sender, EventArgs e)
        {
            if (srcImage == null)
                return;
            pictureBoxConfiguration.Image = srcImage.Bitmap;
            textBoxLocationY.Text = (textBoxLocationY.Text.ToInt() - textBoxMovementOffset.Text.ToInt()).ToString();
            CreateMatrix(sender);
        }

        private void buttonMoveLeft_Click(object sender, EventArgs e)
        {
            if (srcImage == null)
                return;
            pictureBoxConfiguration.Image = srcImage.Bitmap;
            textBoxLocationX.Text = (textBoxLocationX.Text.ToInt() - textBoxMovementOffset.Text.ToInt()).ToString();
            CreateMatrix(sender);
        }

        private void buttonMoveDown_Click(object sender, EventArgs e)
        {
            if (srcImage == null)
                return;
            pictureBoxConfiguration.Image = srcImage.Bitmap;
            textBoxLocationY.Text = (textBoxLocationY.Text.ToInt() + textBoxMovementOffset.Text.ToInt()).ToString();
            CreateMatrix(sender);
        }

        private void buttonMoveRight_Click(object sender, EventArgs e)
        {
            if (srcImage == null)
                return;
            pictureBoxConfiguration.Image = srcImage.Bitmap;
            textBoxLocationX.Text = (textBoxLocationX.Text.ToInt() + textBoxMovementOffset.Text.ToInt()).ToString();
            CreateMatrix(sender);
        }

        private void buttonIncreaseSizeX_Click(object sender, EventArgs e)
        {
            if (srcImage == null)
                return;

            pictureBoxConfiguration.Image = srcImage.Bitmap;
            textBoxWidth.Text = (textBoxWidth.Text.ToInt() + sizeOffset).ToString();
            CreateMatrix(sender);
        }

        private void buttonDecreseSizeY_Click(object sender, EventArgs e)
        {
            if (srcImage == null)
                return;

            pictureBoxConfiguration.Image = srcImage.Bitmap;
            textBoxWidth.Text = (textBoxWidth.Text.ToInt() - sizeOffset).ToString();
            CreateMatrix(sender);
        }

        private void buttonIncreaseSize_Y_Click(object sender, EventArgs e)
        {
            if (srcImage == null)
                return;

            pictureBoxConfiguration.Image = srcImage.Bitmap;
            textBoxHeight.Text = (textBoxHeight.Text.ToInt() + sizeOffset).ToString();
            CreateMatrix(sender);
        }

        private void buttonDecreaseSizeY_Click(object sender, EventArgs e)
        {
            if (srcImage == null)
                return;

            pictureBoxConfiguration.Image = srcImage.Bitmap;
            textBoxHeight.Text = (textBoxHeight.Text.ToInt() - sizeOffset).ToString();
            CreateMatrix(sender);
        }
        #endregion

        #region Private Methods
        private void AllowNumericCharactersOnly(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void ShowException(Exception e, string additionalErrorMessage)
        {
            Stop();
            CloseTheImageProvider();
            MessageBox.Show(e.Message, "Error Occured");
        }

        private void EnableDisableControls()
        {
            if (checkBoxNewProgram.Checked)
            {
                textBoxName.Enabled = true;
                textBoxRows.Enabled = true;
                textBoxColumns.Enabled = true;
                comboBoxPrograms.Enabled = false;
                buttonDeleteProgram.Enabled = false;
            }
            else
            {
                textBoxName.Enabled = false;
                textBoxRows.Enabled = false;
                textBoxColumns.Enabled = false;
                comboBoxPrograms.Enabled = true;
                buttonDeleteProgram.Enabled = true;
            }
        }
        #endregion

        private void checkBoxNewProgram_CheckedChanged(object sender, EventArgs e)
        {
            EnableDisableControls();
        }

        private void CreateProgram_Load(object sender, EventArgs e)
        {
            var availablePrograms = Utility.GetAvailablePrograms();
            availablePrograms.ForEach(program => comboBoxPrograms.Items.Add(program));
            checkBoxNewProgram.Checked = true;
            EnableDisableControls();
            LoadChoosenProgram();
        }

        private void LoadChoosenProgram()
        {
            if (comboBoxPrograms.SelectedItem == null)
                return;
            //ResetImage(Utility.CurrentProgram.RotationAngle);
            ResetImage(m_rotationangle);
                
            var selected = Utility.FetchProgram(comboBoxPrograms.SelectedItem.ToString());
           if (selected != null)
            {
                textBoxName.Text = selected.Name;
                textBoxRows.Text = selected.Rows.ToString();
                textBoxColumns.Text = selected.Columns.ToString();
                textBoxLocationX.Text = selected.Region.X.ToString();
                textBoxLocationY.Text = selected.Region.Y.ToString();
                textBoxWidth.Text = selected.Region.Width.ToString();
                textBoxHeight.Text = selected.Region.Height.ToString();
                m_rotationangle= Utility.CurrentProgram.RotationAngle;
                
                srcImage = srcImage.Rotate(m_rotationangle, new Bgr(), false);
                pictureBoxConfiguration.Image = srcImage.Bitmap;
                ModeToForm();
              
                CreateMatrix(new Button());
                CameraParameters cparams = new CameraParameters(m_imageProvider);
                cparams.setProgramName(comboBoxPrograms.SelectedItem.ToString());
                Utility.LoadProgram(cparams);
                cparams.SetCameraParameters();
             }
        }
      public void  ResetImage(int angle)
        {
             
            srcImage = srcImage.Rotate((360 - angle), new Bgr(), false);
            pictureBoxConfiguration.Image = srcImage.Bitmap;
               
        }
        #region ModeToForm
        private void ModeToForm()
        {
            MaxDMCountToForm();
            MirrorModeToForm();
            LabelModeToForm();
            SpeedModeToForm();
            CellColorToForm();
            QualityModeToForm();
            FilterModeToForm();
            QzModeToForm();
            TimeoutToForm();
        }
      
        private void MirrorModeToForm()
        {
            this.MirrorComboBox.SelectedIndex = Utility.CurrentProgram.Mirror;          
        }
        private void LabelModeToForm()
        {

            this.LabelComboBox.SelectedIndex = Utility.CurrentProgram.LabelMode;
           
        }

        private void SpeedModeToForm()
        {
            this.SpeedComboBox.SelectedIndex = Utility.CurrentProgram.DecodeMode;
            
        }

        private void CellColorToForm()
        {

            this.CellColorComboBox.SelectedIndex = Utility.CurrentProgram.Color;
           
        }

        private void QualityModeToForm()
        {
           
           this.QualityComboBox.SelectedIndex = Utility.CurrentProgram.CalcQuality;
         }
        private void MaxDMCountToForm()
        {
            this.MaxDMCountEdit.Value = Utility.CurrentProgram.MaxDMCount;
            
            }
        private void FilterModeToForm()
        {

            this.FilterComboBox.SelectedIndex = Utility.CurrentProgram.Filter;
           
        }
        private void QzModeToForm()
        {
            this.QzComboBox.SelectedIndex = Utility.CurrentProgram.Quietzone;
           
        }

        private void TimeoutToForm()
        {
            this.TimeOutEdit.Text = Utility.CurrentProgram.Timeout.ToString();
        }

        #endregion

        private void buttonDeleteProgram_Click(object sender, EventArgs e)
        {
            Utility.DeleteProgram(comboBoxPrograms.SelectedItem.ToString());
            CameraParameters cparams = new CameraParameters(m_imageProvider);
            cparams.setProgramName(comboBoxPrograms.SelectedItem.ToString());
            Utility.DeleteProgram(cparams);
            /*var availablePrograms = Utility.GetAvailablePrograms();
            comboBoxPrograms.Items.Clear();
            availablePrograms.ForEach(program => comboBoxPrograms.Items.Add(program));
            comboBoxPrograms.SelectedIndex = -1;
            comboBoxPrograms.SelectedText = string.Empty; */
           
            MessageBox.Show("Program deleted successfully");
            this.Close();
            
         }

        private void comboBoxPrograms_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (pictureBoxConfiguration.Image == null)
            {
                MessageBox.Show("Capture the image before loading the program.");
                comboBoxPrograms.SelectedIndex = -1;
                return;
            }
            pictureBoxConfiguration.Image = srcImage.Bitmap;
            LoadChoosenProgram();
                     
        }

             private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }


        /* Handles the click on the single frame button. */
        private void toolStripButtonOneShot_Click(object sender, EventArgs e)
        {
            OneShot(); /* Starts the grabbing of one image. */
            m_rotationangle = 0;
        }

        /* Handles the click on the continuous frame button. */
        private void toolStripButtonContinuousShot_Click(object sender, EventArgs e)
        {
            ContinuousShot(); /* Start the grabbing of images until grabbing is stopped. */
            m_rotationangle = 0;
        }

        /* Handles the click on the stop frame acquisition button. */
        private void toolStripButtonStop_Click(object sender, EventArgs e)
        {
            Stop(); /* Stops the grabbing of images. */
        }



        /* Updates the list of available devices in the upper left area. */
        private void UpdateDeviceList()
        {
            try
            {
                /* Ask the device enumerator for a list of devices. */
                List<DeviceEnumerator.Device> list = DeviceEnumerator.EnumerateDevices();

                ListView.ListViewItemCollection items = deviceListView.Items;

                /* Add each new device to the list. */
                foreach (DeviceEnumerator.Device device in list)
                {
                    bool newitem = true;
                    /* For each enumerated device check whether it is in the list view. */
                    foreach (ListViewItem item in items)
                    {
                        /* Retrieve the device data from the list view item. */
                        DeviceEnumerator.Device tag = item.Tag as DeviceEnumerator.Device;

                        if (tag.FullName == device.FullName)
                        {
                            /* Update the device index. The index is used for opening the camera. It may change when enumerating devices. */
                            tag.Index = device.Index;
                            /* No new item needs to be added to the list view */
                            newitem = false;
                            break;
                        }
                    }

                    /* If the device is not in the list view yet the add it to the list view. */
                    if (newitem)
                    {
                        ListViewItem item = new ListViewItem(device.Name);
                        if (device.Tooltip.Length > 0)
                        {
                            item.ToolTipText = device.Tooltip;
                        }
                        item.Tag = device;

                        /* Attach the device data. */
                        deviceListView.Items.Add(item);
                    }
                }

                /* Delete old devices which are removed. */
                foreach (ListViewItem item in items)
                {
                    bool exists = false;

                    /* For each device in the list view check whether it has not been found by device enumeration. */
                    foreach (DeviceEnumerator.Device device in list)
                    {
                        if (((DeviceEnumerator.Device)item.Tag).FullName == device.FullName)
                        {
                            exists = true;
                            break;
                        }
                    }
                    /* If the device has not been found by enumeration then remove from the list view. */
                    if (!exists)
                    {
                        deviceListView.Items.Remove(item);
                    }
                }
            }
            catch (Exception e)
            {
                ShowException(e, m_imageProvider.GetLastErrorMessage());
            }
        }


        /* Helps to set the states of all buttons. */
        private void EnableButtons(bool canGrab, bool canStop)
        {
            toolStripButtonContinuousShot.Enabled = canGrab;
            toolStripButtonOneShot.Enabled = canGrab;
            toolStripButtonStop.Enabled = canStop;
        }

        /* Starts the grabbing of one image and handles exceptions. */
        private void OneShot()
        {
            try
            {
                m_imageProvider.OneShot(); /* Starts the grabbing of one image. */

            }
            catch (Exception e)
            {
                ShowException(e, m_imageProvider.GetLastErrorMessage());
            }
        }

        /* Starts the grabbing of images until the grabbing is stopped and handles exceptions. */
        private void ContinuousShot()
        {
            try
            {
                m_imageProvider.ContinuousShot(); /* Start the grabbing of images until grabbing is stopped. */
            }
            catch (Exception e)
            {
                ShowException(e, m_imageProvider.GetLastErrorMessage());
            }
        }

         /* Handles the selection of cameras from the list box. The currently open device is closed and the first
         selected device is opened. */
        private void deviceListView_SelectedIndexChanged(object sender, EventArgs ev)
        {
            /* Close the currently open image provider. */
            /* Stops the grabbing of images. */
            Stop();
            /* Close the image provider. */
            CloseTheImageProvider();

            /* Open the selected image provider. */
            if (deviceListView.SelectedItems.Count > 0)
            {
                /* Get the first selected item. */
                ListViewItem item = deviceListView.SelectedItems[0];
                /* Get the attached device data. */
                DeviceEnumerator.Device device = item.Tag as DeviceEnumerator.Device;
                try
                {
                    /* Open the image provider using the index from the device data. */
                    m_imageProvider.Open(device.Index);
                }
                catch (Exception e)
                {
                    ShowException(e, m_imageProvider.GetLastErrorMessage());
                }
            }
        }

        /* If the F5 key has been pressed update the list of devices. */
        private void deviceListView_KeyDown(object sender, KeyEventArgs ev)
        {
            if (ev.KeyCode == Keys.F5)
            {
                ev.Handled = true;
                /* Update the list of available devices in the upper left area. */
                UpdateDeviceList();
            }
        }

        /* Timer callback used for periodically checking whether displayed devices are still attached to the PC. */
        private void updateDeviceListTimer_Tick(object sender, EventArgs e)
        {
            UpdateDeviceList();
        }

        private void toolStripButtonContinuousShot_Click_1(object sender, EventArgs e)
        {
            ContinuousShot(); /* Start the grabbing of images until grabbing is stopped. */
            
        }

        private void toolStripButtonStop_Click_1(object sender, EventArgs e)
        {
            Stop(); /* Stops the grabbing of images. */
        }

        private void textBoxName_TextChanged(object sender, EventArgs e)
        {
            this.buttonSaveProgram.Enabled = true;
        }

        private void button_Rotate_Click(object sender, EventArgs e)
        {
            RotateImage(90); 
        }
          
        private void  RotateImage(int angle)
        {
            srcImage = srcImage.Rotate(angle,new Bgr() , false);
            m_rotationangle = (m_rotationangle + 90) % 360;
            pictureBoxConfiguration.Image = srcImage.Bitmap;


           /* Bitmap m_bitmap = null; /* The bitmap is used for displaying the image. */
            
           // m_bitmap = (Bitmap)pictureBoxConfiguration.Image;
           // m_bitmap.RotateFlip(RotateFlipType.Rotate90FlipNone);
           // pictureBoxConfiguration.Image = m_bitmap;
           // return m_bitmap;*/

        }

       
    }
}
     