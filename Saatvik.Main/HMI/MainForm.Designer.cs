namespace Saatvik.TwoDMatrix
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.imageListForDeviceList = new System.Windows.Forms.ImageList(this.components);
            this.tblOuterPannel = new System.Windows.Forms.TableLayoutPanel();
            this.tableRightPanel = new System.Windows.Forms.TableLayoutPanel();
            this.panelActionButtons = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelCurrentProgram = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonRunMode = new System.Windows.Forms.Button();
            this.buttonManual = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.labelProcessingTime = new System.Windows.Forms.Label();
            this.labelDecodedCount = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLeftPanel = new System.Windows.Forms.TableLayoutPanel();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCreateNewProgram = new System.Windows.Forms.ToolStripMenuItem();
            this.loadProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBoxPrograms = new System.Windows.Forms.ToolStripComboBox();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.resultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemSetResultsPath = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemViewResults = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBoxMainWindow = new System.Windows.Forms.PictureBox();
            this.labelScanStatus = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.timerAutoMode = new System.Windows.Forms.Timer(this.components);
            this.tblOuterPannel.SuspendLayout();
            this.tableRightPanel.SuspendLayout();
            this.panelActionButtons.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLeftPanel.SuspendLayout();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMainWindow)).BeginInit();
            this.SuspendLayout();
            // 
            // imageListForDeviceList
            // 
            this.imageListForDeviceList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageListForDeviceList.ImageSize = new System.Drawing.Size(32, 32);
            this.imageListForDeviceList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // tblOuterPannel
            // 
            this.tblOuterPannel.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tblOuterPannel.ColumnCount = 2;
            this.tblOuterPannel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblOuterPannel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 370F));
            this.tblOuterPannel.Controls.Add(this.tableRightPanel, 1, 0);
            this.tblOuterPannel.Controls.Add(this.tableLeftPanel, 0, 0);
            this.tblOuterPannel.Controls.Add(this.textBox1, 1, 1);
            this.tblOuterPannel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblOuterPannel.Location = new System.Drawing.Point(0, 0);
            this.tblOuterPannel.Name = "tblOuterPannel";
            this.tblOuterPannel.RowCount = 2;
            this.tblOuterPannel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblOuterPannel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tblOuterPannel.Size = new System.Drawing.Size(1276, 740);
            this.tblOuterPannel.TabIndex = 1;
            // 
            // tableRightPanel
            // 
            this.tableRightPanel.BackColor = System.Drawing.Color.Transparent;
            this.tableRightPanel.ColumnCount = 1;
            this.tableRightPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableRightPanel.Controls.Add(this.panelActionButtons, 0, 1);
            this.tableRightPanel.Controls.Add(this.panel1, 0, 2);
            this.tableRightPanel.Controls.Add(this.pictureBox1, 0, 0);
            this.tableRightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.tableRightPanel.Location = new System.Drawing.Point(909, 3);
            this.tableRightPanel.Name = "tableRightPanel";
            this.tableRightPanel.RowCount = 3;
            this.tableRightPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 87F));
            this.tableRightPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableRightPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableRightPanel.Size = new System.Drawing.Size(364, 701);
            this.tableRightPanel.TabIndex = 0;
            // 
            // panelActionButtons
            // 
            this.panelActionButtons.BackColor = System.Drawing.Color.Transparent;
            this.panelActionButtons.Controls.Add(this.tableLayoutPanel1);
            this.panelActionButtons.Controls.Add(this.buttonRunMode);
            this.panelActionButtons.Controls.Add(this.buttonManual);
            this.panelActionButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelActionButtons.Location = new System.Drawing.Point(3, 90);
            this.panelActionButtons.Name = "panelActionButtons";
            this.panelActionButtons.Size = new System.Drawing.Size(358, 134);
            this.panelActionButtons.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.labelCurrentProgram, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 97);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(358, 37);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // labelCurrentProgram
            // 
            this.labelCurrentProgram.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCurrentProgram.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCurrentProgram.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelCurrentProgram.Location = new System.Drawing.Point(173, 0);
            this.labelCurrentProgram.Name = "labelCurrentProgram";
            this.labelCurrentProgram.Size = new System.Drawing.Size(182, 37);
            this.labelCurrentProgram.TabIndex = 4;
            this.labelCurrentProgram.Text = "[ Not Selected ]";
            this.labelCurrentProgram.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(164, 37);
            this.label2.TabIndex = 3;
            this.label2.Text = "CURRENT PROGRAM:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonRunMode
            // 
            this.buttonRunMode.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonRunMode.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRunMode.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRunMode.Image = global::Saatvik.Properties.Resources.media_play;
            this.buttonRunMode.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonRunMode.Location = new System.Drawing.Point(19, 30);
            this.buttonRunMode.Name = "buttonRunMode";
            this.buttonRunMode.Size = new System.Drawing.Size(144, 50);
            this.buttonRunMode.TabIndex = 2;
            this.buttonRunMode.Text = "ONLINE";
            this.buttonRunMode.UseVisualStyleBackColor = true;
            this.buttonRunMode.Click += new System.EventHandler(this.buttonRunMode_Click);
            // 
            // buttonManual
            // 
            this.buttonManual.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonManual.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonManual.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonManual.Image = global::Saatvik.Properties.Resources.gear_run;
            this.buttonManual.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonManual.Location = new System.Drawing.Point(187, 30);
            this.buttonManual.Name = "buttonManual";
            this.buttonManual.Size = new System.Drawing.Size(144, 50);
            this.buttonManual.TabIndex = 1;
            this.buttonManual.Text = "MANUAL";
            this.buttonManual.UseVisualStyleBackColor = true;
            this.buttonManual.Click += new System.EventHandler(this.buttonManual_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Location = new System.Drawing.Point(3, 234);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(358, 460);
            this.panel1.TabIndex = 3;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(358, 460);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.14773F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.85227F));
            this.tableLayoutPanel3.Controls.Add(this.labelProcessingTime, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelDecodedCount, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 1);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(352, 53);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // labelProcessingTime
            // 
            this.labelProcessingTime.AutoSize = true;
            this.labelProcessingTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelProcessingTime.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProcessingTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelProcessingTime.Location = new System.Drawing.Point(176, 0);
            this.labelProcessingTime.Name = "labelProcessingTime";
            this.labelProcessingTime.Size = new System.Drawing.Size(173, 53);
            this.labelProcessingTime.TabIndex = 1;
            this.labelProcessingTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDecodedCount
            // 
            this.labelDecodedCount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelDecodedCount.Font = new System.Drawing.Font("Segoe UI Symbol", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDecodedCount.ForeColor = System.Drawing.Color.Navy;
            this.labelDecodedCount.Location = new System.Drawing.Point(3, 0);
            this.labelDecodedCount.Name = "labelDecodedCount";
            this.labelDecodedCount.Size = new System.Drawing.Size(167, 53);
            this.labelDecodedCount.TabIndex = 0;
            this.labelDecodedCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::Saatvik.Properties.Resources.Sat_Logo;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(358, 81);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // tableLeftPanel
            // 
            this.tableLeftPanel.ColumnCount = 1;
            this.tableLeftPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLeftPanel.Controls.Add(this.menuStrip, 0, 0);
            this.tableLeftPanel.Controls.Add(this.pictureBoxMainWindow, 2, 2);
            this.tableLeftPanel.Controls.Add(this.labelScanStatus, 0, 1);
            this.tableLeftPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLeftPanel.Location = new System.Drawing.Point(3, 3);
            this.tableLeftPanel.Name = "tableLeftPanel";
            this.tableLeftPanel.RowCount = 3;
            this.tableLeftPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLeftPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLeftPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLeftPanel.Size = new System.Drawing.Size(900, 701);
            this.tableLeftPanel.TabIndex = 1;
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.resultsToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(900, 22);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuCreateNewProgram,
            this.loadProgramToolStripMenuItem,
            this.exitToolStripMenuItem1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 18);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // menuCreateNewProgram
            // 
            this.menuCreateNewProgram.Name = "menuCreateNewProgram";
            this.menuCreateNewProgram.Size = new System.Drawing.Size(176, 22);
            this.menuCreateNewProgram.Text = "Add / Edit Program";
            this.menuCreateNewProgram.Click += new System.EventHandler(this.menuCreateNewProgram_Click);
            // 
            // loadProgramToolStripMenuItem
            // 
            this.loadProgramToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBoxPrograms});
            this.loadProgramToolStripMenuItem.Name = "loadProgramToolStripMenuItem";
            this.loadProgramToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.loadProgramToolStripMenuItem.Text = "Load Program";
            // 
            // toolStripComboBoxPrograms
            // 
            this.toolStripComboBoxPrograms.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.toolStripComboBoxPrograms.Name = "toolStripComboBoxPrograms";
            this.toolStripComboBoxPrograms.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBoxPrograms.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBoxPrograms_SelectedIndexChanged);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(176, 22);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // resultsToolStripMenuItem
            // 
            this.resultsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemSetResultsPath,
            this.menuItemViewResults});
            this.resultsToolStripMenuItem.Name = "resultsToolStripMenuItem";
            this.resultsToolStripMenuItem.Size = new System.Drawing.Size(56, 18);
            this.resultsToolStripMenuItem.Text = "Results";
            // 
            // menuItemSetResultsPath
            // 
            this.menuItemSetResultsPath.Name = "menuItemSetResultsPath";
            this.menuItemSetResultsPath.Size = new System.Drawing.Size(154, 22);
            this.menuItemSetResultsPath.Text = "Set results path";
            this.menuItemSetResultsPath.Click += new System.EventHandler(this.menuItemSetResultsPath_Click);
            // 
            // menuItemViewResults
            // 
            this.menuItemViewResults.Name = "menuItemViewResults";
            this.menuItemViewResults.Size = new System.Drawing.Size(154, 22);
            this.menuItemViewResults.Text = "View results";
            this.menuItemViewResults.Click += new System.EventHandler(this.menuItemViewResults_Click);
            // 
            // pictureBoxMainWindow
            // 
            this.pictureBoxMainWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxMainWindow.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxMainWindow.Location = new System.Drawing.Point(6, 84);
            this.pictureBoxMainWindow.Margin = new System.Windows.Forms.Padding(6);
            this.pictureBoxMainWindow.Name = "pictureBoxMainWindow";
            this.pictureBoxMainWindow.Padding = new System.Windows.Forms.Padding(30);
            this.pictureBoxMainWindow.Size = new System.Drawing.Size(888, 611);
            this.pictureBoxMainWindow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxMainWindow.TabIndex = 0;
            this.pictureBoxMainWindow.TabStop = false;
            // 
            // labelScanStatus
            // 
            this.labelScanStatus.BackColor = System.Drawing.Color.Green;
            this.labelScanStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelScanStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelScanStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScanStatus.ForeColor = System.Drawing.Color.White;
            this.labelScanStatus.Location = new System.Drawing.Point(3, 31);
            this.labelScanStatus.Margin = new System.Windows.Forms.Padding(3, 9, 3, 6);
            this.labelScanStatus.Name = "labelScanStatus";
            this.labelScanStatus.Size = new System.Drawing.Size(894, 41);
            this.labelScanStatus.TabIndex = 2;
            this.labelScanStatus.Text = "GOOD";
            this.labelScanStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(1190, 710);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(83, 20);
            this.textBox1.TabIndex = 2;
            this.textBox1.Text = "Version 6.0.3";
            // 
            // timerAutoMode
            // 
            this.timerAutoMode.Interval = 500;
            this.timerAutoMode.Tick += new System.EventHandler(this.timerAutoMode_Tick);
            // 
            // MainForm
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1276, 740);
            this.Controls.Add(this.tblOuterPannel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "2D Matrix";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tblOuterPannel.ResumeLayout(false);
            this.tblOuterPannel.PerformLayout();
            this.tableRightPanel.ResumeLayout(false);
            this.panelActionButtons.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLeftPanel.ResumeLayout(false);
            this.tableLeftPanel.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMainWindow)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxMainWindow;
        private System.Windows.Forms.ImageList imageListForDeviceList;
        private System.Windows.Forms.TableLayoutPanel tblOuterPannel;
        private System.Windows.Forms.TableLayoutPanel tableRightPanel;
        private System.Windows.Forms.TableLayoutPanel tableLeftPanel;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuCreateNewProgram;
        private System.Windows.Forms.ToolStripMenuItem resultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItemSetResultsPath;
        private System.Windows.Forms.ToolStripMenuItem menuItemViewResults;
        private System.Windows.Forms.Button buttonManual;
        private System.Windows.Forms.Panel panelActionButtons;
        private System.Windows.Forms.Button buttonRunMode;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelScanStatus;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem loadProgramToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxPrograms;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelCurrentProgram;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label labelProcessingTime;
        private System.Windows.Forms.Label labelDecodedCount;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Timer timerAutoMode;

    }
}

