using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;
using Saatvik;
using PylonC.NETSupportLibrary;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using Emgu.CV;
using Emgu.CV.Structure;
using _2DMatrixCodeDecoder;
using Saatvik.Globals;
using Saatvik.Entities;
using System.Linq;
using Saatvik.HMI;
using PylonC.NET;
using icDMDecNetPro;

namespace Saatvik.TwoDMatrix
{
    /* The main window. */
    public partial class MainForm : Form
    {
        private static ImageProvider m_imageProvider = new ImageProvider();
        private static CameraParameters c_cameraparameters = null;
        private static _2DMatrixCodeDecoder.CodeDecoder CodeDecoder = null;
        private int m_rotationangle = 0;
                      
        public MainForm()
        {
            InitializeComponent();

            if (CodeDecoder == null)
            {
                CodeDecoder = new _2DMatrixCodeDecoder.CodeDecoder();
            }
            
           c_cameraparameters = new CameraParameters(m_imageProvider);
                                
            //Open Camera
            //if (PylonC.NET.Pylon.EnumerateDevices() != 0)
            //    m_imageProvider.Open(0);

            /* Enable the tool strip buttons according to the state of the image provider. */
            // EnableButtons(m_imageProvider.IsOpen, false);
        }

        #region Image Grabber Events
        private static DateTime ProcessStarTime = DateTime.MinValue;
        public delegate void ProcessImageHandler();
        private void OnImageReadyEventCallback()
        {
            if (!AppData.IsFolderPathConfigured())
            {
                MessageBox.Show("Failed to scan. Please configure results path!");
                if (Utility.IsOnlineModeEnabled)
                {
                    Utility.IsOnlineModeEnabled = false;
                    EnableDisableControls(true);
                }
                return;
            }

            if (InvokeRequired)
            {
                /* If called from a different thread, we must use the Invoke method to marshal the call to the proper thread. */
                BeginInvoke(new ProcessImageHandler(OnImageReadyEventCallback));
                return;
            }
            Utility.IsBusy = true;
            Image image = GrabImage();
            
            ProcessStarTime = DateTime.Now;
            /* Check if the image has been removed in the meantime. */
            if (image != null)
            {
                                   
                 Bitmap m_bitmap = null;
                /* Check if the image is compatible with the currently used bitmap. */
                if (BitmapFactory.IsCompatible(m_bitmap, image.Width, image.Height, image.Color))
                {
                    /* Update the bitmap with the image data. */
                    BitmapFactory.UpdateBitmap(m_bitmap, image.Buffer, image.Width, image.Height, image.Color);
                }
                else /* A new bitmap is required. */
                {
                    BitmapFactory.CreateBitmap(out m_bitmap, image.Width, image.Height, image.Color);
                    BitmapFactory.UpdateBitmap(m_bitmap, image.Buffer, image.Width, image.Height, image.Color);
                }

                 var srcImage = new Image<Bgr, byte>(m_bitmap);
                //Image<Bgr, byte> resizedImage = srcImage.Resize(pictureBoxMainWindow.Width, pictureBoxMainWindow.Height, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);

                m_rotationangle = Utility.CurrentProgram.RotationAngle;
                srcImage = srcImage.Rotate(m_rotationangle, new Bgr(), false);
                pictureBoxMainWindow.Image = srcImage.Bitmap;


                try
                {
                    if (m_bitmap != null)
                    {
                        if (CodeDecoder != null)
                        {
                            var codes = new List<string>();
                            var matchedPoints = new List<DecodedPoint>();
                            using (Image<Bgr, byte> imageMat = new Image<Bgr, byte>(new Bitmap(pictureBoxMainWindow.Image)))
                            {
                                imageMat.ROI = Utility.CurrentProgram.Region;
                                FormToMode();
                                pictureBoxMainWindow.Image = CodeDecoder.ProcessImageAndDecode(imageMat.Bitmap, ref codes, ref matchedPoints);
                            }
                            labelDecodedCount.Text = "Decoded Count: " + codes.Count;

                            if (Utility.codeResultsFolderPath != string.Empty && Directory.Exists(Utility.codeResultsFolderPath))
                            {
                                if (codes.Count == Utility.CurrentProgram.Rows * Utility.CurrentProgram.Columns)
                                {
                                    var path = Utility.codeResultsFolderPath + "\\" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
                                    var fileStream = File.Create(path);
                                    fileStream.Close();

                                    using (StreamWriter sw = new StreamWriter(path))
                                    {
                                        codes.ForEach(code => sw.WriteLine(code));
                                    }
                                    labelScanStatus.Text = "Success";
                                    labelScanStatus.BackColor = Color.Green;
                                }
                                else
                                {
                                    labelScanStatus.Text = "Failed";
                                    labelScanStatus.BackColor = Color.Red;
                                }
                            }
                            CreateMatrix(matchedPoints);
                        }

                        pictureBoxMainWindow.Refresh();
                        labelProcessingTime.Text = Math.Round(DateTime.Now.Subtract(ProcessStarTime).TotalMilliseconds, 2) + " MS";
                    }
                }
                catch (Exception ex)
                {
                }
            }
            Utility.IsBusy = false;
        }
        #endregion

        private void HandleCameraException(string errorMessage)
        {
            TerminateCameraHandle();
            Utility.IsOnlineModeEnabled = false;
            timerAutoMode.Enabled = false;
            EnableDisableControls(true);
            MessageBox.Show(errorMessage);
        }

        private void EnableDisableControls(bool enabled)
        {
            fileToolStripMenuItem.Enabled = enabled;
            resultsToolStripMenuItem.Enabled = enabled;
            buttonManual.Enabled = enabled;
        }

        private static void TerminateCameraHandle()
        {
            try
            {
                if (hDev.IsValid)
                {
                    /* ... Close and release the pylon device. */
                    if (Pylon.DeviceIsOpen(hDev))
                    {
                        Pylon.DeviceClose(hDev);
                    }
                    Pylon.DestroyDevice(hDev);
                    hDev = null;
                }
            }
            catch (Exception)
            {
                /*No further handling here.*/
            }
        }

             #region FormToMode

        public void FormToMode()
        {
            FormToMaxDMCount();
            FormToMirrorMode();
            FormToLabelMode();
            FormToSpeedMode();
            FormToCellColor();
            FormToQualityMode();
            FormToFilterMode();
            FormToQzMode();
            FormToTimeout();
        }
               private void FormToMirrorMode()
        {
            switch (Utility.CurrentProgram.Mirror)
            {
                case 0: CodeDecoder.Decoder.MirrorMode = DM_MIRROR_MODE.MM_NORMAL; break;
                case 1: CodeDecoder.Decoder.MirrorMode = DM_MIRROR_MODE.MM_MIRROR; break;
                case 2: CodeDecoder.Decoder.MirrorMode = DM_MIRROR_MODE.MM_ANY; break;
            }    
               }
               private void FormToLabelMode()
               {
                   switch (Utility.CurrentProgram.LabelMode)
                   {
                       case 0: CodeDecoder.Decoder.LabelMode = DM_LABEL_MODE.LM_STANDARD; break;
                       case 1: CodeDecoder.Decoder.LabelMode = DM_LABEL_MODE.LM_DOTPEEN; break;
                       case 2: CodeDecoder.Decoder.LabelMode = DM_LABEL_MODE.LM_FAX; break;
                       case 3: CodeDecoder.Decoder.LabelMode = DM_LABEL_MODE.LM_ST_DOT; break;
                   }
               }

               private void FormToSpeedMode()
               {
                   switch (Utility.CurrentProgram.DecodeMode)
                   {
                       case 0: CodeDecoder.Decoder.SpeedMode = (int)DM_SPEED.DMSP_ULTIMATE; break;
                       case 1: CodeDecoder.Decoder.SpeedMode = (int)DM_SPEED.DMSP_REGULAR; break;
                       case 2: CodeDecoder.Decoder.SpeedMode = (int)DM_SPEED.DMSP_EXPRESS; break;
                       case 3: CodeDecoder.Decoder.SpeedMode = (int)DM_DECODER_SPEED.SP_FAST; break;
                   }
               }

               private void FormToCellColor()
               {
                   switch (Utility.CurrentProgram.Color)
                   {
                       case 0:CodeDecoder.Decoder.CellColor = DM_CELL_COLOR.CL_BLACKONWHITE; break;
                       case 1: CodeDecoder.Decoder.CellColor = DM_CELL_COLOR.CL_WHITEONBLACK; break;
                       case 2: CodeDecoder.Decoder.CellColor = DM_CELL_COLOR.CL_ANY; break;
                   }
               }

               private void FormToQualityMode()
               {
                   switch (Utility.CurrentProgram.CalcQuality)
                   {
                       case 0: CodeDecoder.Decoder.QualityMask = DM_QUALITY_MASK.DM_QM_NO; break;
                       default: CodeDecoder.Decoder.QualityMask = DM_QUALITY_MASK.DM_QM_ALL; break;
                   }
               }
               private void FormToMaxDMCount()
               {
                   CodeDecoder.Decoder.MaxDMCount = (int)Utility.CurrentProgram.MaxDMCount;
               }

               private void FormToFilterMode( )
               {
                   switch (Utility.CurrentProgram.Filter)
                   {
                       case 0: CodeDecoder.Decoder.FilterMode = DM_FILTER_MODE.FM_NON; break;
                       case 1: CodeDecoder.Decoder.FilterMode = DM_FILTER_MODE.FM_SHARP1; break;
                       case 2: CodeDecoder.Decoder.FilterMode = DM_FILTER_MODE.FM_SHARP2; break;
                       case 3: CodeDecoder.Decoder.FilterMode = DM_FILTER_MODE.FM_SHARPMASK; break;
                       case 4: CodeDecoder.Decoder.FilterMode = DM_FILTER_MODE.FM_AUTO; break;
                       case 5: CodeDecoder.Decoder.FilterMode = DM_FILTER_MODE.FM_BWR; break;
                       case 6: CodeDecoder.Decoder.FilterMode = DM_FILTER_MODE.FM_SM_BWR; break;
                   }
               }

               private void FormToQzMode()
               {
                   switch (Utility.CurrentProgram.Quietzone)
                   {
                       case 0:CodeDecoder.Decoder.QzMode = (int)DMQZ_MODE.DMQZ_NORMAL; break;
                       case 1: CodeDecoder.Decoder.QzMode = (int)DMQZ_MODE.DMQZ_SMALL; break;
                   }
               }
               private void FormToTimeout()
               {
                   CodeDecoder.Decoder.Timeout = (int)Utility.CurrentProgram.Timeout;
               }

              
               #endregion

        #region Form Events
        private void MainForm_FormClosing(object sender, FormClosingEventArgs ev)
        {
            TerminateCameraHandle();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Utility.codeResultsFolderPath = AppData.ReadDataPath();
            if (Utility.codeResultsFolderPath == string.Empty)
            {
                menuItemViewResults.Enabled = false;
            }

            if (Utility.CurrentProgram.Name == null)
            {
                buttonManual.Enabled = false;
                buttonRunMode.Enabled = false;
            }
        }

        private void menuCreateNewProgram_Click(object sender, EventArgs e)
        {
            if (Utility.AccessLevelType != Utility.AccessLevelTypes.Admin)
            {
                new FormPassword(ShowCreateProgramWindow).Show();
            }
            else
                ShowCreateProgramWindow();
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                toolStripComboBoxPrograms.Items.Clear();
                var availablePrograms = Utility.GetAvailablePrograms();
                availablePrograms.ForEach(program => toolStripComboBoxPrograms.Items.Add(program));
            }
            catch
            {
                MessageBox.Show("Failed to load available programs");
            }
        }

        private void toolStripComboBoxPrograms_SelectedIndexChanged(object sender, EventArgs e)
        {
                  
          if (Utility.CurrentProgram != null && toolStripComboBoxPrograms.SelectedItem.Equals(Utility.CurrentProgram.Name))
            {
                return;
            }
            
            fileToolStripMenuItem.HideDropDown();
            //fetch saved program details
            Utility.LoadProgram(toolStripComboBoxPrograms.SelectedItem + ".sat");
            labelCurrentProgram.Text = toolStripComboBoxPrograms.SelectedItem.ToString();

            CameraParameters cparams = new CameraParameters(m_imageProvider);
            cparams.setProgramName(toolStripComboBoxPrograms.SelectedItem.ToString());
            Utility.LoadProgram(cparams);

            //Close any existing camera handler before setting Camera Parameters
            TerminateCameraHandle();
            if (PylonC.NET.Pylon.EnumerateDevices() != 0)
            {
                m_imageProvider.Open(0);
                cparams.SetCameraParameters();
                m_imageProvider.Close();
            }
            buttonManual.Enabled = true;
            buttonRunMode.Enabled = true;

            MessageBox.Show("Program loaded successfully");
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void menuItemSetResultsPath_Click(object sender, EventArgs e)
        {
            try
            {
                if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    Utility.codeResultsFolderPath = folderBrowserDialog.SelectedPath;
                    menuItemViewResults.Enabled = true;
                    AppData.SaveDataPath(Utility.codeResultsFolderPath);
                }
            }
            catch
            {
                MessageBox.Show("Unable to set the results path folder");
            }
        }

        private void menuItemViewResults_Click(object sender, EventArgs e)
        {
            try
            {
                if (AppData.IsFolderPathConfigured())
                {
                    Process.Start(Utility.codeResultsFolderPath);
                }
                else
                    MessageBox.Show("Results Folder Not Found!!");
            }
            catch
            {
                MessageBox.Show("Loading folder containing results has failed");
            }
        }

        private void buttonManual_Click(object sender, EventArgs e)
        {
            Utility.IsOnlineModeEnabled = false;
            var selectedProgram = Utility.FetchProgram(this.labelCurrentProgram.Text);
            if (selectedProgram.Name != null)
            {
                OnImageReadyEventCallback();
            }
            else
                MessageBox.Show("Program " +"\""+this.labelCurrentProgram.Text +"\""+ " doesnot Exists");
        }

        private void buttonRunMode_Click(object sender, EventArgs e)
        {
            buttonRunMode.Text = Utility.IsOnlineModeEnabled ? "OFFLINE" : "ONLINE";
            var selectedProgram = Utility.FetchProgram(this.labelCurrentProgram.Text);
            if (selectedProgram.Name != null)
            {
                Utility.IsOnlineModeEnabled = !Utility.IsOnlineModeEnabled;
                timerAutoMode.Enabled = Utility.IsOnlineModeEnabled;
                EnableDisableControls(!Utility.IsOnlineModeEnabled);
             }
            else
                MessageBox.Show("Program " +"\""+this.labelCurrentProgram.Text +"\""+ " doesnot Exists");
        }
        #endregion

        private void ShowCreateProgramWindow()
        {
            TerminateCameraHandle();
            var formProgram = new CreateProgram(this);
            formProgram.Show();
        }

        public void LoadPgmToMainForm(String PgmName)
        {
            Utility.LoadProgram(PgmName + ".sat");

            labelCurrentProgram.Text = PgmName;
            buttonManual.Enabled = true;
            buttonRunMode.Enabled = true;
            MessageBox.Show("Program " + "\"" + PgmName + "\"" + " loaded successfully");
        }

        private void CreateMatrix(List<DecodedPoint> matchedPoints)
        {
            try
            {
                int rows = Utility.CurrentProgram.Rows, columns = Utility.CurrentProgram.Columns;
                if (rows == 0 || columns == 0 || pictureBoxMainWindow.Image == null)
                {
                    return;
                }

                var width = Utility.CurrentProgram.Region.Width / columns;
                var height = Utility.CurrentProgram.Region.Height / rows;
                Image<Bgr, byte> imageMat = new Image<Bgr, byte>(new Bitmap(pictureBoxMainWindow.Image));
                for (int i = 0; i < rows; i++)
                {
                    for (int k = 0; k < columns; k++)
                    {
                        //Archana commenting..
                        //int x = Utility.CurrentProgram.Region.Location.X + k * width, y = Utility.CurrentProgram.Region.Location.Y + i * height;
                        int x = k * width, y = i * height;
                        var matched = matchedPoints.Find(p => p.X1 > x && p.X1 < x + width && p.Y1 > y && p.Y1 < y + height);
                        pictureBoxMainWindow.Image = EmguUtility.DrawRectangle(imageMat, new Point(x, y), width, height, matched);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static void ConvertCoordinates(PictureBox pic, out int X0, out int Y0, int x, int y)
        {
            int pic_hgt = pic.ClientSize.Height;
            int pic_wid = pic.ClientSize.Width;
            int img_hgt = pic.Image.Height;
            int img_wid = pic.Image.Width;
            X0 = x;
            Y0 = y;

            switch (pic.SizeMode)
            {
                case PictureBoxSizeMode.AutoSize:
                case PictureBoxSizeMode.Normal:
                    break;
                case PictureBoxSizeMode.CenterImage:
                    X0 = x - (pic_wid - img_wid) / 2;
                    Y0 = y - (pic_hgt - img_hgt) / 2;
                    break;
                case PictureBoxSizeMode.StretchImage:
                    X0 = (int)(img_wid * x / (float)pic_wid);
                    Y0 = (int)(img_hgt * y / (float)pic_hgt);
                    break;
                case PictureBoxSizeMode.Zoom:

                    float pic_aspect = pic_wid / (float)pic_hgt;

                    float img_aspect = img_wid / (float)img_wid;

                    if (pic_aspect > img_aspect)
                    {

                        // The PictureBox is wider/shorter than the image.

                        Y0 = (int)(img_hgt * y / (float)pic_hgt);



                        // The image fills the height of the PictureBox.

                        // Get its width.

                        float scaled_width = img_wid * pic_hgt / img_hgt;

                        float dx = (pic_wid - scaled_width) / 2;

                        X0 = (int)((x - dx) * img_hgt / (float)pic_hgt);

                    }

                    else
                    {

                        // The PictureBox is taller/thinner than the image.

                        X0 = (int)(img_wid * x / (float)pic_wid);



                        // The image fills the height of the PictureBox.

                        // Get its height.

                        float scaled_height = img_hgt * pic_wid / img_wid;

                        float dy = (pic_hgt - scaled_height) / 2;

                        Y0 = (int)((y - dy) * img_wid / pic_wid);

                    }

                    break;

            }

        }

        #region Camera Operation related methods
        static PYLON_DEVICE_HANDLE hDev = new PYLON_DEVICE_HANDLE();
        public void SetupGrab()
        {
            //If camera handle is already defined
            if (hDev != null && hDev.IsValid)
                return;

            var numDevices = Pylon.EnumerateDevices();
            if (0 == numDevices)
            {
                throw new Exception("No devices found.");
            }

            CameraParameters cparams = new CameraParameters(m_imageProvider);
            cparams.setProgramName(Utility.CurrentProgram.Name);

            //Load saved camera parameters for current program
            Utility.LoadProgram(cparams);

            //open and set the camera parameters
            m_imageProvider.Open(0);
            cparams.SetCameraParameters();
            m_imageProvider.Close();

            //create camera handle
            hDev = Pylon.CreateDeviceByIndex(0);
            Pylon.DeviceOpen(hDev, Pylon.cPylonAccessModeControl | Pylon.cPylonAccessModeStream);
        }

        public Image GrabImage()
        {
            Image grabbedImage = null;
            PylonBuffer<Byte> imgBuf = null;
            try
            {
                SetupGrab();

                //Check whether camera is open
                if (!Pylon.DeviceIsOpen(hDev))
                {
                    hDev = null;
                    throw new Exception("Camera is not connected!");
                }
                
                PylonGrabResult_t grabResult;
                if (!Pylon.DeviceGrabSingleFrame(hDev, 0, ref imgBuf, out grabResult,500))
                {
                    throw new Exception("Frame {0}: timeout.");
                }

                //If image grabbed successfully, then convert to image from buffer
                if (grabResult.Status == EPylonGrabStatus.Grabbed)
                {
                    grabbedImage = new Image(grabResult.SizeX, grabResult.SizeY, imgBuf.Array, grabResult.PixelType == EPylonPixelType.PixelType_RGBA8packed);
                }

                /*Archana commenting the exception 
                 * else
                 {
                    throw new Exception("Failed to grab the image! Please check camera paramteres");
                 }*/

                //Releasae the memory allocated to image buffer to avoid memory leaks
                imgBuf.Dispose();
            }
            catch (Exception ex)
            {
                if (imgBuf != null) imgBuf.Dispose();
                HandleCameraException(ex.Message);
            }
            return grabbedImage;
        }

        private void timerAutoMode_Tick(object sender, EventArgs e)
        {
            if (!Utility.IsBusy)
                OnImageReadyEventCallback();
        }
        #endregion
        }

    public class Image
    {
        public Image(int newWidth, int newHeight, Byte[] newBuffer, bool color)
        {
            Width = newWidth;
            Height = newHeight;
            Buffer = newBuffer;
            Color = color;
        }
        public readonly int Width; /* The width of the image. */
        public readonly int Height; /* The height of the image. */
        public readonly Byte[] Buffer; /* The raw image data. */
        public readonly bool Color; /* If false the buffer contains a Mono8 image. Otherwise, RGBA8packed is provided. */
    }
}