﻿using Saatvik.Globals;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Saatvik.HMI
{
    public partial class FormPassword : Form
    {
        private Action _callBackOnSubmit = null;
        public FormPassword(Action callBackOnSubmit)
        {
            _callBackOnSubmit = callBackOnSubmit;
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPassword.Text) || !txtPassword.Text.Equals("Admin"))
            {
                txtPassword.Text = string.Empty;
                labelError.Text = "** Invalid password. Please enter the correct password";
                return;
            }
            Utility.AccessLevelType = Utility.AccessLevelTypes.Admin;

            if (_callBackOnSubmit != null)
                _callBackOnSubmit();
            this.Close();
        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            labelError.Text = string.Empty;
        }
    }
}
