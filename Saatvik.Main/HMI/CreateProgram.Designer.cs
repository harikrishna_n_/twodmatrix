﻿namespace Saatvik
{
    partial class CreateProgram
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            if (srcImage != null) srcImage.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ErpProgram = new System.Windows.Forms.ErrorProvider(this.components);
            this.tblOuterLayer = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBoxConfiguration = new System.Windows.Forms.PictureBox();
            this.tblHeader = new System.Windows.Forms.TableLayoutPanel();
            this.tblHeaderContent = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxColumns = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxRows = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBoxNewProgram = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.comboBoxPrograms = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonCaptureImage = new System.Windows.Forms.Button();
            this.btnCreateMatrix = new System.Windows.Forms.Button();
            this.panelImageControls = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.buttonDeleteProgram = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonIncreaseSize_Y = new System.Windows.Forms.Button();
            this.buttonDecreaseSizeY = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonIncreaseSize_X = new System.Windows.Forms.Button();
            this.buttonDecreaseSize_X = new System.Windows.Forms.Button();
            this.textBoxHeight = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxWidth = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxMovementOffset = new System.Windows.Forms.TextBox();
            this.textBoxLocationY = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxLocationX = new System.Windows.Forms.TextBox();
            this.buttonMoveUp = new System.Windows.Forms.Button();
            this.buttonMoveLeft = new System.Windows.Forms.Button();
            this.buttonMoveRight = new System.Windows.Forms.Button();
            this.buttonMoveDown = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.TimeOutEdit = new System.Windows.Forms.NumericUpDown();
            this.label35 = new System.Windows.Forms.Label();
            this.QzComboBox = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.FilterComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.CellColorComboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.QualityComboBox = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.LabelComboBox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.SpeedComboBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.MaxDMCountEdit = new System.Windows.Forms.NumericUpDown();
            this.MirrorComboBox = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.sliderPacketSize = new PylonC.NETSupportLibrary.SliderUserControl();
            this.vScrollBar2 = new System.Windows.Forms.VScrollBar();
            this.sliderInterPacketDelay = new PylonC.NETSupportLibrary.SliderUserControl();
            this.sliderWidth = new PylonC.NETSupportLibrary.SliderUserControl();
            this.sliderHeight = new PylonC.NETSupportLibrary.SliderUserControl();
            this.sliderGain = new PylonC.NETSupportLibrary.SliderUserControl();
            this.sliderExposureTime = new PylonC.NETSupportLibrary.SliderUserControl();
            this.comboBoxShutterMode = new PylonC.NETSupportLibrary.EnumerationComboBoxUserControl();
            this.comboBoxPixelFormat = new PylonC.NETSupportLibrary.EnumerationComboBoxUserControl();
            this.comboBoxTestImage = new PylonC.NETSupportLibrary.EnumerationComboBoxUserControl();
            this.deviceListView = new System.Windows.Forms.ListView();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonOneShot = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonContinuousShot = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonStop = new System.Windows.Forms.ToolStripButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button_Rotate = new System.Windows.Forms.Button();
            this.buttonSaveProgram = new System.Windows.Forms.Button();
            this.updateDeviceListTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ErpProgram)).BeginInit();
            this.tblOuterLayer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxConfiguration)).BeginInit();
            this.tblHeader.SuspendLayout();
            this.tblHeaderContent.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.panelImageControls.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimeOutEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxDMCountEdit)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // ErpProgram
            // 
            this.ErpProgram.ContainerControl = this;
            // 
            // tblOuterLayer
            // 
            this.tblOuterLayer.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tblOuterLayer.ColumnCount = 3;
            this.tblOuterLayer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 387F));
            this.tblOuterLayer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblOuterLayer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 319F));
            this.tblOuterLayer.Controls.Add(this.pictureBoxConfiguration, 1, 1);
            this.tblOuterLayer.Controls.Add(this.tblHeader, 0, 0);
            this.tblOuterLayer.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tblOuterLayer.Controls.Add(this.panel2, 2, 1);
            this.tblOuterLayer.Controls.Add(this.panel4, 1, 2);
            this.tblOuterLayer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblOuterLayer.Location = new System.Drawing.Point(0, 0);
            this.tblOuterLayer.Name = "tblOuterLayer";
            this.tblOuterLayer.RowCount = 3;
            this.tblOuterLayer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblOuterLayer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 614F));
            this.tblOuterLayer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tblOuterLayer.Size = new System.Drawing.Size(1336, 705);
            this.tblOuterLayer.TabIndex = 4;
            // 
            // pictureBoxConfiguration
            // 
            this.pictureBoxConfiguration.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxConfiguration.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxConfiguration.Location = new System.Drawing.Point(409, 71);
            this.pictureBoxConfiguration.Margin = new System.Windows.Forms.Padding(20);
            this.pictureBoxConfiguration.Name = "pictureBoxConfiguration";
            this.pictureBoxConfiguration.Padding = new System.Windows.Forms.Padding(30);
            this.pictureBoxConfiguration.Size = new System.Drawing.Size(586, 574);
            this.pictureBoxConfiguration.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxConfiguration.TabIndex = 2;
            this.pictureBoxConfiguration.TabStop = false;
            // 
            // tblHeader
            // 
            this.tblHeader.ColumnCount = 1;
            this.tblOuterLayer.SetColumnSpan(this.tblHeader, 3);
            this.tblHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblHeader.Controls.Add(this.tblHeaderContent, 0, 0);
            this.tblHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblHeader.Location = new System.Drawing.Point(4, 4);
            this.tblHeader.Name = "tblHeader";
            this.tblHeader.RowCount = 1;
            this.tblHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblHeader.Size = new System.Drawing.Size(1328, 43);
            this.tblHeader.TabIndex = 5;
            // 
            // tblHeaderContent
            // 
            this.tblHeaderContent.ColumnCount = 3;
            this.tblHeaderContent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblHeaderContent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblHeaderContent.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tblHeaderContent.Controls.Add(this.tableLayoutPanel4, 2, 0);
            this.tblHeaderContent.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tblHeaderContent.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tblHeaderContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblHeaderContent.Location = new System.Drawing.Point(3, 3);
            this.tblHeaderContent.Name = "tblHeaderContent";
            this.tblHeaderContent.RowCount = 1;
            this.tblHeaderContent.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblHeaderContent.Size = new System.Drawing.Size(1322, 37);
            this.tblHeaderContent.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56F));
            this.tableLayoutPanel4.Controls.Add(this.textBoxColumns, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(883, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(436, 31);
            this.tableLayoutPanel4.TabIndex = 4;
            // 
            // textBoxColumns
            // 
            this.textBoxColumns.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxColumns.Location = new System.Drawing.Point(194, 5);
            this.textBoxColumns.Name = "textBoxColumns";
            this.textBoxColumns.Size = new System.Drawing.Size(90, 20);
            this.textBoxColumns.TabIndex = 5;
            this.textBoxColumns.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllowNumericCharactersOnly);
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(185, 31);
            this.label3.TabIndex = 4;
            this.label3.Text = "Columns";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56F));
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.textBoxRows, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(443, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(434, 31);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(184, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "Rows";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxRows
            // 
            this.textBoxRows.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxRows.Location = new System.Drawing.Point(193, 5);
            this.textBoxRows.Name = "textBoxRows";
            this.textBoxRows.Size = new System.Drawing.Size(83, 20);
            this.textBoxRows.TabIndex = 3;
            this.textBoxRows.TextChanged += new System.EventHandler(this.textBoxRows_TextChanged);
            this.textBoxRows.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllowNumericCharactersOnly);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56F));
            this.tableLayoutPanel2.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxPrograms, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(434, 31);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBoxNewProgram);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBoxName);
            this.panel1.Location = new System.Drawing.Point(193, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(151, 25);
            this.panel1.TabIndex = 5;
            // 
            // checkBoxNewProgram
            // 
            this.checkBoxNewProgram.AutoSize = true;
            this.checkBoxNewProgram.Location = new System.Drawing.Point(6, 6);
            this.checkBoxNewProgram.Name = "checkBoxNewProgram";
            this.checkBoxNewProgram.Size = new System.Drawing.Size(15, 14);
            this.checkBoxNewProgram.TabIndex = 3;
            this.checkBoxNewProgram.UseVisualStyleBackColor = true;
            this.checkBoxNewProgram.CheckedChanged += new System.EventHandler(this.checkBoxNewProgram_CheckedChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(1, -3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxName
            // 
            this.textBoxName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBoxName.Location = new System.Drawing.Point(65, 3);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(136, 20);
            this.textBoxName.TabIndex = 2;
            this.textBoxName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxName.TextChanged += new System.EventHandler(this.textBoxName_TextChanged);
            // 
            // comboBoxPrograms
            // 
            this.comboBoxPrograms.FormattingEnabled = true;
            this.comboBoxPrograms.Location = new System.Drawing.Point(3, 3);
            this.comboBoxPrograms.Name = "comboBoxPrograms";
            this.comboBoxPrograms.Size = new System.Drawing.Size(117, 21);
            this.comboBoxPrograms.TabIndex = 6;
            this.comboBoxPrograms.SelectedIndexChanged += new System.EventHandler(this.comboBoxPrograms_SelectedIndexChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoScroll = true;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panelImageControls, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel7, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 54);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.4013F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 84.5987F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 164F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(381, 608);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.buttonCaptureImage, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.btnCreateMatrix, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(375, 62);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // buttonCaptureImage
            // 
            this.buttonCaptureImage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonCaptureImage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCaptureImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCaptureImage.Image = global::Saatvik.Properties.Resources.camera;
            this.buttonCaptureImage.Location = new System.Drawing.Point(52, 11);
            this.buttonCaptureImage.Name = "buttonCaptureImage";
            this.buttonCaptureImage.Size = new System.Drawing.Size(83, 40);
            this.buttonCaptureImage.TabIndex = 3;
            this.buttonCaptureImage.UseVisualStyleBackColor = true;
            this.buttonCaptureImage.Click += new System.EventHandler(this.buttonCaptureImage_Click);
            // 
            // btnCreateMatrix
            // 
            this.btnCreateMatrix.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCreateMatrix.Location = new System.Drawing.Point(227, 14);
            this.btnCreateMatrix.Name = "btnCreateMatrix";
            this.btnCreateMatrix.Size = new System.Drawing.Size(108, 34);
            this.btnCreateMatrix.TabIndex = 4;
            this.btnCreateMatrix.Text = "Create Matrix";
            this.btnCreateMatrix.UseVisualStyleBackColor = true;
            this.btnCreateMatrix.Click += new System.EventHandler(this.btnCreateMatrix_Click);
            // 
            // panelImageControls
            // 
            this.panelImageControls.Controls.Add(this.panel5);
            this.panelImageControls.Controls.Add(this.panel6);
            this.panelImageControls.Controls.Add(this.groupBox2);
            this.panelImageControls.Controls.Add(this.groupBox1);
            this.panelImageControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelImageControls.Location = new System.Drawing.Point(3, 71);
            this.panelImageControls.Name = "panelImageControls";
            this.panelImageControls.Size = new System.Drawing.Size(375, 369);
            this.panelImageControls.TabIndex = 5;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.buttonDeleteProgram);
            this.panel5.Location = new System.Drawing.Point(6, 325);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(340, 39);
            this.panel5.TabIndex = 17;
            // 
            // buttonDeleteProgram
            // 
            this.buttonDeleteProgram.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonDeleteProgram.Location = new System.Drawing.Point(98, 3);
            this.buttonDeleteProgram.Name = "buttonDeleteProgram";
            this.buttonDeleteProgram.Size = new System.Drawing.Size(137, 34);
            this.buttonDeleteProgram.TabIndex = 14;
            this.buttonDeleteProgram.Text = "Delete Program";
            this.buttonDeleteProgram.UseVisualStyleBackColor = true;
            this.buttonDeleteProgram.Click += new System.EventHandler(this.buttonDeleteProgram_Click);
            // 
            // panel6
            // 
            this.panel6.Location = new System.Drawing.Point(6, 397);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(200, 100);
            this.panel6.TabIndex = 16;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonIncreaseSize_Y);
            this.groupBox2.Controls.Add(this.buttonDecreaseSizeY);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.buttonIncreaseSize_X);
            this.groupBox2.Controls.Add(this.buttonDecreaseSize_X);
            this.groupBox2.Controls.Add(this.textBoxHeight);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.textBoxWidth);
            this.groupBox2.Location = new System.Drawing.Point(9, 184);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(337, 135);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dimensions";
            // 
            // buttonIncreaseSize_Y
            // 
            this.buttonIncreaseSize_Y.Image = global::Saatvik.Properties.Resources.document_zoom_in;
            this.buttonIncreaseSize_Y.Location = new System.Drawing.Point(130, 85);
            this.buttonIncreaseSize_Y.Name = "buttonIncreaseSize_Y";
            this.buttonIncreaseSize_Y.Size = new System.Drawing.Size(48, 41);
            this.buttonIncreaseSize_Y.TabIndex = 13;
            this.buttonIncreaseSize_Y.UseVisualStyleBackColor = true;
            this.buttonIncreaseSize_Y.Click += new System.EventHandler(this.buttonIncreaseSize_Y_Click);
            // 
            // buttonDecreaseSizeY
            // 
            this.buttonDecreaseSizeY.Image = global::Saatvik.Properties.Resources.document_zoom_out;
            this.buttonDecreaseSizeY.Location = new System.Drawing.Point(184, 85);
            this.buttonDecreaseSizeY.Name = "buttonDecreaseSizeY";
            this.buttonDecreaseSizeY.Size = new System.Drawing.Size(48, 41);
            this.buttonDecreaseSizeY.TabIndex = 14;
            this.buttonDecreaseSizeY.UseVisualStyleBackColor = true;
            this.buttonDecreaseSizeY.Click += new System.EventHandler(this.buttonDecreaseSizeY_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 95);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Height:";
            // 
            // buttonIncreaseSize_X
            // 
            this.buttonIncreaseSize_X.Image = global::Saatvik.Properties.Resources.document_zoom_in;
            this.buttonIncreaseSize_X.Location = new System.Drawing.Point(130, 33);
            this.buttonIncreaseSize_X.Name = "buttonIncreaseSize_X";
            this.buttonIncreaseSize_X.Size = new System.Drawing.Size(48, 41);
            this.buttonIncreaseSize_X.TabIndex = 9;
            this.buttonIncreaseSize_X.UseVisualStyleBackColor = true;
            this.buttonIncreaseSize_X.Click += new System.EventHandler(this.buttonIncreaseSizeX_Click);
            // 
            // buttonDecreaseSize_X
            // 
            this.buttonDecreaseSize_X.Image = global::Saatvik.Properties.Resources.document_zoom_out;
            this.buttonDecreaseSize_X.Location = new System.Drawing.Point(184, 33);
            this.buttonDecreaseSize_X.Name = "buttonDecreaseSize_X";
            this.buttonDecreaseSize_X.Size = new System.Drawing.Size(48, 41);
            this.buttonDecreaseSize_X.TabIndex = 10;
            this.buttonDecreaseSize_X.UseVisualStyleBackColor = true;
            this.buttonDecreaseSize_X.Click += new System.EventHandler(this.buttonDecreseSizeY_Click);
            // 
            // textBoxHeight
            // 
            this.textBoxHeight.Location = new System.Drawing.Point(76, 91);
            this.textBoxHeight.Name = "textBoxHeight";
            this.textBoxHeight.Size = new System.Drawing.Size(36, 20);
            this.textBoxHeight.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Width:";
            // 
            // textBoxWidth
            // 
            this.textBoxWidth.Location = new System.Drawing.Point(76, 48);
            this.textBoxWidth.Name = "textBoxWidth";
            this.textBoxWidth.Size = new System.Drawing.Size(36, 20);
            this.textBoxWidth.TabIndex = 20;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBoxMovementOffset);
            this.groupBox1.Controls.Add(this.textBoxLocationY);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxLocationX);
            this.groupBox1.Controls.Add(this.buttonMoveUp);
            this.groupBox1.Controls.Add(this.buttonMoveLeft);
            this.groupBox1.Controls.Add(this.buttonMoveRight);
            this.groupBox1.Controls.Add(this.buttonMoveDown);
            this.groupBox1.Location = new System.Drawing.Point(9, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(338, 163);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Step 1: Location";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Y:";
            // 
            // textBoxMovementOffset
            // 
            this.textBoxMovementOffset.Location = new System.Drawing.Point(201, 72);
            this.textBoxMovementOffset.Multiline = true;
            this.textBoxMovementOffset.Name = "textBoxMovementOffset";
            this.textBoxMovementOffset.Size = new System.Drawing.Size(36, 24);
            this.textBoxMovementOffset.TabIndex = 14;
            this.textBoxMovementOffset.Text = "50";
            // 
            // textBoxLocationY
            // 
            this.textBoxLocationY.Location = new System.Drawing.Point(43, 70);
            this.textBoxLocationY.Name = "textBoxLocationY";
            this.textBoxLocationY.Size = new System.Drawing.Size(36, 20);
            this.textBoxLocationY.TabIndex = 4;
            this.textBoxLocationY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllowNumericCharactersOnly);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "X:";
            // 
            // textBoxLocationX
            // 
            this.textBoxLocationX.Location = new System.Drawing.Point(43, 40);
            this.textBoxLocationX.Name = "textBoxLocationX";
            this.textBoxLocationX.Size = new System.Drawing.Size(36, 20);
            this.textBoxLocationX.TabIndex = 2;
            this.textBoxLocationX.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllowNumericCharactersOnly);
            // 
            // buttonMoveUp
            // 
            this.buttonMoveUp.Image = global::Saatvik.Properties.Resources.nav_up;
            this.buttonMoveUp.Location = new System.Drawing.Point(197, 15);
            this.buttonMoveUp.Name = "buttonMoveUp";
            this.buttonMoveUp.Size = new System.Drawing.Size(48, 41);
            this.buttonMoveUp.TabIndex = 13;
            this.buttonMoveUp.UseVisualStyleBackColor = true;
            this.buttonMoveUp.Click += new System.EventHandler(this.buttonMoveUp_Click);
            // 
            // buttonMoveLeft
            // 
            this.buttonMoveLeft.Image = global::Saatvik.Properties.Resources.nav_left;
            this.buttonMoveLeft.Location = new System.Drawing.Point(148, 62);
            this.buttonMoveLeft.Name = "buttonMoveLeft";
            this.buttonMoveLeft.Size = new System.Drawing.Size(48, 41);
            this.buttonMoveLeft.TabIndex = 17;
            this.buttonMoveLeft.UseVisualStyleBackColor = true;
            this.buttonMoveLeft.Click += new System.EventHandler(this.buttonMoveLeft_Click);
            // 
            // buttonMoveRight
            // 
            this.buttonMoveRight.Image = global::Saatvik.Properties.Resources.nav_right;
            this.buttonMoveRight.Location = new System.Drawing.Point(243, 61);
            this.buttonMoveRight.Name = "buttonMoveRight";
            this.buttonMoveRight.Size = new System.Drawing.Size(48, 41);
            this.buttonMoveRight.TabIndex = 15;
            this.buttonMoveRight.UseVisualStyleBackColor = true;
            this.buttonMoveRight.Click += new System.EventHandler(this.buttonMoveRight_Click);
            // 
            // buttonMoveDown
            // 
            this.buttonMoveDown.Image = global::Saatvik.Properties.Resources.nav_down;
            this.buttonMoveDown.Location = new System.Drawing.Point(197, 111);
            this.buttonMoveDown.Name = "buttonMoveDown";
            this.buttonMoveDown.Size = new System.Drawing.Size(48, 41);
            this.buttonMoveDown.TabIndex = 16;
            this.buttonMoveDown.UseVisualStyleBackColor = true;
            this.buttonMoveDown.Click += new System.EventHandler(this.buttonMoveDown_Click);
            // 
            // panel7
            // 
            this.panel7.AutoScroll = true;
            this.panel7.Controls.Add(this.TimeOutEdit);
            this.panel7.Controls.Add(this.label35);
            this.panel7.Controls.Add(this.QzComboBox);
            this.panel7.Controls.Add(this.label29);
            this.panel7.Controls.Add(this.label28);
            this.panel7.Controls.Add(this.FilterComboBox);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Controls.Add(this.CellColorComboBox);
            this.panel7.Controls.Add(this.label8);
            this.panel7.Controls.Add(this.QualityComboBox);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.LabelComboBox);
            this.panel7.Controls.Add(this.label11);
            this.panel7.Controls.Add(this.SpeedComboBox);
            this.panel7.Controls.Add(this.label12);
            this.panel7.Controls.Add(this.label13);
            this.panel7.Controls.Add(this.MaxDMCountEdit);
            this.panel7.Controls.Add(this.MirrorComboBox);
            this.panel7.Location = new System.Drawing.Point(3, 446);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(346, 159);
            this.panel7.TabIndex = 6;
            // 
            // TimeOutEdit
            // 
            this.TimeOutEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TimeOutEdit.Increment = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.TimeOutEdit.Location = new System.Drawing.Point(213, 227);
            this.TimeOutEdit.Maximum = new decimal(new int[] {
            16000,
            0,
            0,
            0});
            this.TimeOutEdit.Name = "TimeOutEdit";
            this.TimeOutEdit.Size = new System.Drawing.Size(62, 20);
            this.TimeOutEdit.TabIndex = 38;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label35.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label35.Location = new System.Drawing.Point(46, 230);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(50, 13);
            this.label35.TabIndex = 37;
            this.label35.Text = "Time Out";
            // 
            // QzComboBox
            // 
            this.QzComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "No",
            "Yes"});
            this.QzComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.QzComboBox.FormattingEnabled = true;
            this.QzComboBox.Items.AddRange(new object[] {
            "Normal",
            "Small"});
            this.QzComboBox.Location = new System.Drawing.Point(154, 200);
            this.QzComboBox.Name = "QzComboBox";
            this.QzComboBox.Size = new System.Drawing.Size(121, 21);
            this.QzComboBox.TabIndex = 36;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label29.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label29.Location = new System.Drawing.Point(46, 201);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(60, 13);
            this.label29.TabIndex = 35;
            this.label29.Text = "Quiet Zone";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label28.Location = new System.Drawing.Point(46, 173);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(29, 13);
            this.label28.TabIndex = 34;
            this.label28.Text = "Filter";
            // 
            // FilterComboBox
            // 
            this.FilterComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "No",
            "Yes"});
            this.FilterComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FilterComboBox.FormattingEnabled = true;
            this.FilterComboBox.Items.AddRange(new object[] {
            "NON",
            "Sharp1",
            "Sharp2",
            "SharpMask",
            "AUTO",
            "BWR",
            "SM+BWR"});
            this.FilterComboBox.Location = new System.Drawing.Point(154, 172);
            this.FilterComboBox.Name = "FilterComboBox";
            this.FilterComboBox.Size = new System.Drawing.Size(121, 21);
            this.FilterComboBox.TabIndex = 33;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label9.Location = new System.Drawing.Point(46, 148);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "Color";
            // 
            // CellColorComboBox
            // 
            this.CellColorComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "Black on White",
            "White on Black",
            "Any"});
            this.CellColorComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellColorComboBox.FormattingEnabled = true;
            this.CellColorComboBox.Items.AddRange(new object[] {
            "Black on White",
            "White on Black",
            "Any"});
            this.CellColorComboBox.Location = new System.Drawing.Point(154, 144);
            this.CellColorComboBox.Name = "CellColorComboBox";
            this.CellColorComboBox.Size = new System.Drawing.Size(121, 21);
            this.CellColorComboBox.TabIndex = 31;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label8.Location = new System.Drawing.Point(44, 120);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 30;
            this.label8.Text = "Calc Quality";
            // 
            // QualityComboBox
            // 
            this.QualityComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "No",
            "Yes"});
            this.QualityComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.QualityComboBox.FormattingEnabled = true;
            this.QualityComboBox.Items.AddRange(new object[] {
            "No",
            "Yes"});
            this.QualityComboBox.Location = new System.Drawing.Point(199, 116);
            this.QualityComboBox.Name = "QualityComboBox";
            this.QualityComboBox.Size = new System.Drawing.Size(76, 21);
            this.QualityComboBox.TabIndex = 29;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label10.Location = new System.Drawing.Point(46, 92);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "Label Mode";
            // 
            // LabelComboBox
            // 
            this.LabelComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "Standard",
            "Dot peen",
            "Fax"});
            this.LabelComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelComboBox.FormattingEnabled = true;
            this.LabelComboBox.Items.AddRange(new object[] {
            "Standard",
            "Dot peen",
            "Fax",
            "St+Dot"});
            this.LabelComboBox.Location = new System.Drawing.Point(154, 88);
            this.LabelComboBox.Name = "LabelComboBox";
            this.LabelComboBox.Size = new System.Drawing.Size(121, 21);
            this.LabelComboBox.TabIndex = 27;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label11.Location = new System.Drawing.Point(46, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Decode Mode";
            // 
            // SpeedComboBox
            // 
            this.SpeedComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "Ultimate",
            "Regular",
            "Express",
            "Fast"});
            this.SpeedComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SpeedComboBox.FormattingEnabled = true;
            this.SpeedComboBox.Items.AddRange(new object[] {
            "Ultimate",
            "Regular",
            "Express",
            "Fast"});
            this.SpeedComboBox.Location = new System.Drawing.Point(154, 61);
            this.SpeedComboBox.Name = "SpeedComboBox";
            this.SpeedComboBox.Size = new System.Drawing.Size(121, 21);
            this.SpeedComboBox.TabIndex = 25;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label12.Location = new System.Drawing.Point(46, 38);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Mirror";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label13.Location = new System.Drawing.Point(46, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "max DM Count";
            // 
            // MaxDMCountEdit
            // 
            this.MaxDMCountEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaxDMCountEdit.Location = new System.Drawing.Point(213, 8);
            this.MaxDMCountEdit.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.MaxDMCountEdit.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MaxDMCountEdit.Name = "MaxDMCountEdit";
            this.MaxDMCountEdit.Size = new System.Drawing.Size(62, 20);
            this.MaxDMCountEdit.TabIndex = 22;
            this.MaxDMCountEdit.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // MirrorComboBox
            // 
            this.MirrorComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "Normal",
            "Mirror",
            "Any"});
            this.MirrorComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MirrorComboBox.FormattingEnabled = true;
            this.MirrorComboBox.Items.AddRange(new object[] {
            "Normal",
            "Mirror",
            "Normal & Mirror"});
            this.MirrorComboBox.Location = new System.Drawing.Point(154, 34);
            this.MirrorComboBox.Name = "MirrorComboBox";
            this.MirrorComboBox.Size = new System.Drawing.Size(121, 21);
            this.MirrorComboBox.TabIndex = 21;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.deviceListView);
            this.panel2.Controls.Add(this.toolStrip);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(1019, 54);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(313, 608);
            this.panel2.TabIndex = 7;
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.Controls.Add(this.sliderPacketSize);
            this.panel3.Controls.Add(this.vScrollBar2);
            this.panel3.Controls.Add(this.sliderInterPacketDelay);
            this.panel3.Controls.Add(this.sliderWidth);
            this.panel3.Controls.Add(this.sliderHeight);
            this.panel3.Controls.Add(this.sliderGain);
            this.panel3.Controls.Add(this.sliderExposureTime);
            this.panel3.Controls.Add(this.comboBoxShutterMode);
            this.panel3.Controls.Add(this.comboBoxPixelFormat);
            this.panel3.Controls.Add(this.comboBoxTestImage);
            this.panel3.Location = new System.Drawing.Point(6, 215);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(309, 393);
            this.panel3.TabIndex = 3;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // sliderPacketSize
            // 
            this.sliderPacketSize.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sliderPacketSize.Location = new System.Drawing.Point(4, 391);
            this.sliderPacketSize.MaximumSize = new System.Drawing.Size(875, 50);
            this.sliderPacketSize.MinimumSize = new System.Drawing.Size(225, 50);
            this.sliderPacketSize.Name = "sliderPacketSize";
            this.sliderPacketSize.NodeName = "PacketSize";
            this.sliderPacketSize.Size = new System.Drawing.Size(243, 50);
            this.sliderPacketSize.TabIndex = 17;
            // 
            // vScrollBar2
            // 
            this.vScrollBar2.Location = new System.Drawing.Point(296, 0);
            this.vScrollBar2.Name = "vScrollBar2";
            this.vScrollBar2.Size = new System.Drawing.Size(14, 393);
            this.vScrollBar2.TabIndex = 16;
            // 
            // sliderInterPacketDelay
            // 
            this.sliderInterPacketDelay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sliderInterPacketDelay.Location = new System.Drawing.Point(3, 343);
            this.sliderInterPacketDelay.MaximumSize = new System.Drawing.Size(875, 50);
            this.sliderInterPacketDelay.MinimumSize = new System.Drawing.Size(225, 50);
            this.sliderInterPacketDelay.Name = "sliderInterPacketDelay";
            this.sliderInterPacketDelay.NodeName = "InterPacketDelay";
            this.sliderInterPacketDelay.Size = new System.Drawing.Size(257, 50);
            this.sliderInterPacketDelay.TabIndex = 14;
            // 
            // sliderWidth
            // 
            this.sliderWidth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sliderWidth.Location = new System.Drawing.Point(3, 298);
            this.sliderWidth.MaximumSize = new System.Drawing.Size(875, 50);
            this.sliderWidth.MinimumSize = new System.Drawing.Size(225, 50);
            this.sliderWidth.Name = "sliderWidth";
            this.sliderWidth.NodeName = "Width";
            this.sliderWidth.Size = new System.Drawing.Size(244, 50);
            this.sliderWidth.TabIndex = 13;
            // 
            // sliderHeight
            // 
            this.sliderHeight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sliderHeight.Location = new System.Drawing.Point(3, 242);
            this.sliderHeight.MaximumSize = new System.Drawing.Size(875, 50);
            this.sliderHeight.MinimumSize = new System.Drawing.Size(225, 50);
            this.sliderHeight.Name = "sliderHeight";
            this.sliderHeight.NodeName = "Height";
            this.sliderHeight.Size = new System.Drawing.Size(244, 50);
            this.sliderHeight.TabIndex = 12;
            // 
            // sliderGain
            // 
            this.sliderGain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sliderGain.Location = new System.Drawing.Point(3, 186);
            this.sliderGain.MaximumSize = new System.Drawing.Size(875, 50);
            this.sliderGain.MinimumSize = new System.Drawing.Size(225, 50);
            this.sliderGain.Name = "sliderGain";
            this.sliderGain.NodeName = "GainRaw";
            this.sliderGain.Size = new System.Drawing.Size(244, 50);
            this.sliderGain.TabIndex = 11;
            // 
            // sliderExposureTime
            // 
            this.sliderExposureTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sliderExposureTime.Location = new System.Drawing.Point(0, 130);
            this.sliderExposureTime.MaximumSize = new System.Drawing.Size(875, 50);
            this.sliderExposureTime.MinimumSize = new System.Drawing.Size(225, 50);
            this.sliderExposureTime.Name = "sliderExposureTime";
            this.sliderExposureTime.NodeName = "ExposureTimeRaw";
            this.sliderExposureTime.Size = new System.Drawing.Size(244, 50);
            this.sliderExposureTime.TabIndex = 10;
            // 
            // comboBoxShutterMode
            // 
            this.comboBoxShutterMode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxShutterMode.Location = new System.Drawing.Point(3, 88);
            this.comboBoxShutterMode.MinimumSize = new System.Drawing.Size(150, 39);
            this.comboBoxShutterMode.Name = "comboBoxShutterMode";
            this.comboBoxShutterMode.NodeName = "ShutterMode";
            this.comboBoxShutterMode.Size = new System.Drawing.Size(169, 46);
            this.comboBoxShutterMode.TabIndex = 9;
            // 
            // comboBoxPixelFormat
            // 
            this.comboBoxPixelFormat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxPixelFormat.Location = new System.Drawing.Point(3, 45);
            this.comboBoxPixelFormat.MinimumSize = new System.Drawing.Size(150, 39);
            this.comboBoxPixelFormat.Name = "comboBoxPixelFormat";
            this.comboBoxPixelFormat.NodeName = "PixelFormat";
            this.comboBoxPixelFormat.Size = new System.Drawing.Size(169, 46);
            this.comboBoxPixelFormat.TabIndex = 8;
            // 
            // comboBoxTestImage
            // 
            this.comboBoxTestImage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxTestImage.Location = new System.Drawing.Point(3, 3);
            this.comboBoxTestImage.MinimumSize = new System.Drawing.Size(150, 39);
            this.comboBoxTestImage.Name = "comboBoxTestImage";
            this.comboBoxTestImage.NodeName = "TestImageSelector";
            this.comboBoxTestImage.Size = new System.Drawing.Size(169, 46);
            this.comboBoxTestImage.TabIndex = 7;
            // 
            // deviceListView
            // 
            this.deviceListView.Location = new System.Drawing.Point(3, 42);
            this.deviceListView.MultiSelect = false;
            this.deviceListView.Name = "deviceListView";
            this.deviceListView.ShowItemToolTips = true;
            this.deviceListView.Size = new System.Drawing.Size(306, 167);
            this.deviceListView.TabIndex = 2;
            this.deviceListView.UseCompatibleStateImageBehavior = false;
            this.deviceListView.View = System.Windows.Forms.View.Tile;
            this.deviceListView.SelectedIndexChanged += new System.EventHandler(this.deviceListView_SelectedIndexChanged);
            // 
            // toolStrip
            // 
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonOneShot,
            this.toolStripButtonContinuousShot,
            this.toolStripButtonStop});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(313, 39);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip";
            // 
            // toolStripButtonOneShot
            // 
            this.toolStripButtonOneShot.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonOneShot.Image = global::Saatvik.Properties.Resources.OneShot;
            this.toolStripButtonOneShot.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonOneShot.Name = "toolStripButtonOneShot";
            this.toolStripButtonOneShot.Size = new System.Drawing.Size(36, 36);
            this.toolStripButtonOneShot.Text = "One Shot";
            this.toolStripButtonOneShot.ToolTipText = "One Shot";
            this.toolStripButtonOneShot.Click += new System.EventHandler(this.toolStripButtonOneShot_Click);
            // 
            // toolStripButtonContinuousShot
            // 
            this.toolStripButtonContinuousShot.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonContinuousShot.Image = global::Saatvik.Properties.Resources.ContinuousShot;
            this.toolStripButtonContinuousShot.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonContinuousShot.Name = "toolStripButtonContinuousShot";
            this.toolStripButtonContinuousShot.Size = new System.Drawing.Size(36, 36);
            this.toolStripButtonContinuousShot.Text = "Continuous Shot";
            this.toolStripButtonContinuousShot.ToolTipText = "Continuous Shot";
            this.toolStripButtonContinuousShot.Click += new System.EventHandler(this.toolStripButtonContinuousShot_Click_1);
            // 
            // toolStripButtonStop
            // 
            this.toolStripButtonStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonStop.Image = global::Saatvik.Properties.Resources.Stop;
            this.toolStripButtonStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonStop.Name = "toolStripButtonStop";
            this.toolStripButtonStop.Size = new System.Drawing.Size(36, 36);
            this.toolStripButtonStop.Text = "Stop Grab";
            this.toolStripButtonStop.ToolTipText = "Stop Grab";
            this.toolStripButtonStop.Click += new System.EventHandler(this.toolStripButtonStop_Click_1);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button_Rotate);
            this.panel4.Controls.Add(this.buttonSaveProgram);
            this.panel4.Location = new System.Drawing.Point(392, 669);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(603, 32);
            this.panel4.TabIndex = 8;
            // 
            // button_Rotate
            // 
            this.button_Rotate.Location = new System.Drawing.Point(356, 0);
            this.button_Rotate.Name = "button_Rotate";
            this.button_Rotate.Size = new System.Drawing.Size(115, 33);
            this.button_Rotate.TabIndex = 2;
            this.button_Rotate.Text = "Rotate";
            this.button_Rotate.UseVisualStyleBackColor = true;
            this.button_Rotate.Click += new System.EventHandler(this.button_Rotate_Click);
            // 
            // buttonSaveProgram
            // 
            this.buttonSaveProgram.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.buttonSaveProgram.Location = new System.Drawing.Point(147, -1);
            this.buttonSaveProgram.Name = "buttonSaveProgram";
            this.buttonSaveProgram.Size = new System.Drawing.Size(108, 32);
            this.buttonSaveProgram.TabIndex = 1;
            this.buttonSaveProgram.Text = "Save";
            this.buttonSaveProgram.UseVisualStyleBackColor = true;
            this.buttonSaveProgram.Click += new System.EventHandler(this.buttonSaveProgram_Click);
            // 
            // updateDeviceListTimer
            // 
            this.updateDeviceListTimer.Enabled = true;
            this.updateDeviceListTimer.Interval = 5000;
            this.updateDeviceListTimer.Tick += new System.EventHandler(this.updateDeviceListTimer_Tick);
            // 
            // CreateProgram
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(1336, 705);
            this.Controls.Add(this.tblOuterLayer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "CreateProgram";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Program";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CreateProgram_FormClosing);
            this.Load += new System.EventHandler(this.CreateProgram_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ErpProgram)).EndInit();
            this.tblOuterLayer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxConfiguration)).EndInit();
            this.tblHeader.ResumeLayout(false);
            this.tblHeaderContent.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.panelImageControls.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimeOutEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxDMCountEdit)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxConfiguration;
        private System.Windows.Forms.ErrorProvider ErpProgram;
        private System.Windows.Forms.TableLayoutPanel tblOuterLayer;
        private System.Windows.Forms.Button buttonSaveProgram;
        private System.Windows.Forms.TableLayoutPanel tblHeader;
        private System.Windows.Forms.TableLayoutPanel tblHeaderContent;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TextBox textBoxColumns;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxRows;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkBoxNewProgram;
        private System.Windows.Forms.ComboBox comboBoxPrograms;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton toolStripButtonOneShot;
        private System.Windows.Forms.ToolStripButton toolStripButtonContinuousShot;
        private System.Windows.Forms.ToolStripButton toolStripButtonStop;
        private System.Windows.Forms.ListView deviceListView;
        private System.Windows.Forms.Panel panel3;
        private PylonC.NETSupportLibrary.EnumerationComboBoxUserControl comboBoxTestImage;
        private PylonC.NETSupportLibrary.EnumerationComboBoxUserControl comboBoxPixelFormat;
        private PylonC.NETSupportLibrary.EnumerationComboBoxUserControl comboBoxShutterMode;
        private PylonC.NETSupportLibrary.SliderUserControl sliderExposureTime;
        private PylonC.NETSupportLibrary.SliderUserControl sliderGain;
        private PylonC.NETSupportLibrary.SliderUserControl sliderHeight;
        private PylonC.NETSupportLibrary.SliderUserControl sliderWidth;
        private PylonC.NETSupportLibrary.SliderUserControl sliderInterPacketDelay;
        private System.Windows.Forms.VScrollBar vScrollBar2;
        private PylonC.NETSupportLibrary.SliderUserControl sliderPacketSize;
        private System.Windows.Forms.Timer updateDeviceListTimer;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button_Rotate;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button buttonCaptureImage;
        private System.Windows.Forms.Button btnCreateMatrix;
        private System.Windows.Forms.Panel panelImageControls;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button buttonDeleteProgram;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonIncreaseSize_Y;
        private System.Windows.Forms.Button buttonDecreaseSizeY;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonIncreaseSize_X;
        private System.Windows.Forms.Button buttonDecreaseSize_X;
        private System.Windows.Forms.TextBox textBoxHeight;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxWidth;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxMovementOffset;
        private System.Windows.Forms.TextBox textBoxLocationY;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxLocationX;
        private System.Windows.Forms.Button buttonMoveUp;
        private System.Windows.Forms.Button buttonMoveLeft;
        private System.Windows.Forms.Button buttonMoveRight;
        private System.Windows.Forms.Button buttonMoveDown;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.NumericUpDown TimeOutEdit;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox QzComboBox;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox FilterComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox CellColorComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox QualityComboBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox LabelComboBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox SpeedComboBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown MaxDMCountEdit;
        private System.Windows.Forms.ComboBox MirrorComboBox;
    }
}