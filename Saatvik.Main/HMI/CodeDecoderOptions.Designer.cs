﻿namespace Saatvik.HMI
{
    partial class CodeDecoderOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OptionsGroupBox = new System.Windows.Forms.GroupBox();
            this.TimeOutEdit = new System.Windows.Forms.NumericUpDown();
            this.label35 = new System.Windows.Forms.Label();
            this.QzComboBox = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.FilterComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.CellColorComboBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.QualityComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.LabelComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SpeedComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.MaxDMCountEdit = new System.Windows.Forms.NumericUpDown();
            this.MirrorComboBox = new System.Windows.Forms.ComboBox();
            this.OptionsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimeOutEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxDMCountEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // OptionsGroupBox
            // 
            this.OptionsGroupBox.Controls.Add(this.TimeOutEdit);
            this.OptionsGroupBox.Controls.Add(this.label35);
            this.OptionsGroupBox.Controls.Add(this.QzComboBox);
            this.OptionsGroupBox.Controls.Add(this.label29);
            this.OptionsGroupBox.Controls.Add(this.label28);
            this.OptionsGroupBox.Controls.Add(this.FilterComboBox);
            this.OptionsGroupBox.Controls.Add(this.label9);
            this.OptionsGroupBox.Controls.Add(this.CellColorComboBox);
            this.OptionsGroupBox.Controls.Add(this.label8);
            this.OptionsGroupBox.Controls.Add(this.QualityComboBox);
            this.OptionsGroupBox.Controls.Add(this.label7);
            this.OptionsGroupBox.Controls.Add(this.LabelComboBox);
            this.OptionsGroupBox.Controls.Add(this.label6);
            this.OptionsGroupBox.Controls.Add(this.SpeedComboBox);
            this.OptionsGroupBox.Controls.Add(this.label5);
            this.OptionsGroupBox.Controls.Add(this.label4);
            this.OptionsGroupBox.Controls.Add(this.MaxDMCountEdit);
            this.OptionsGroupBox.Controls.Add(this.MirrorComboBox);
            this.OptionsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OptionsGroupBox.ForeColor = System.Drawing.SystemColors.Desktop;
            this.OptionsGroupBox.Location = new System.Drawing.Point(12, 12);
            this.OptionsGroupBox.Name = "OptionsGroupBox";
            this.OptionsGroupBox.Size = new System.Drawing.Size(261, 275);
            this.OptionsGroupBox.TabIndex = 1;
            this.OptionsGroupBox.TabStop = false;
            this.OptionsGroupBox.Text = "Options";
            // 
            // TimeOutEdit
            // 
            this.TimeOutEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TimeOutEdit.Increment = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.TimeOutEdit.Location = new System.Drawing.Point(173, 244);
            this.TimeOutEdit.Maximum = new decimal(new int[] {
            16000,
            0,
            0,
            0});
            this.TimeOutEdit.Name = "TimeOutEdit";
            this.TimeOutEdit.Size = new System.Drawing.Size(62, 20);
            this.TimeOutEdit.TabIndex = 20;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label35.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label35.Location = new System.Drawing.Point(6, 247);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(58, 13);
            this.label35.TabIndex = 19;
            this.label35.Text = "Time Out";
            // 
            // QzComboBox
            // 
            this.QzComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "No",
            "Yes"});
            this.QzComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.QzComboBox.FormattingEnabled = true;
            this.QzComboBox.Items.AddRange(new object[] {
            "Normal",
            "Small"});
            this.QzComboBox.Location = new System.Drawing.Point(114, 217);
            this.QzComboBox.Name = "QzComboBox";
            this.QzComboBox.Size = new System.Drawing.Size(121, 21);
            this.QzComboBox.TabIndex = 17;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label29.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label29.Location = new System.Drawing.Point(6, 218);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(70, 13);
            this.label29.TabIndex = 16;
            this.label29.Text = "Quiet Zone";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label28.Location = new System.Drawing.Point(6, 190);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(35, 13);
            this.label28.TabIndex = 15;
            this.label28.Text = "Filter";
            // 
            // FilterComboBox
            // 
            this.FilterComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "No",
            "Yes"});
            this.FilterComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FilterComboBox.FormattingEnabled = true;
            this.FilterComboBox.Items.AddRange(new object[] {
            "NON",
            "Sharp1",
            "Sharp2",
            "SharpMask",
            "AUTO",
            "BWR",
            "SM+BWR"});
            this.FilterComboBox.Location = new System.Drawing.Point(114, 189);
            this.FilterComboBox.Name = "FilterComboBox";
            this.FilterComboBox.Size = new System.Drawing.Size(121, 21);
            this.FilterComboBox.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label9.Location = new System.Drawing.Point(6, 165);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Color";
            // 
            // CellColorComboBox
            // 
            this.CellColorComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "Black on White",
            "White on Black",
            "Any"});
            this.CellColorComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellColorComboBox.FormattingEnabled = true;
            this.CellColorComboBox.Items.AddRange(new object[] {
            "Black on White",
            "White on Black",
            "Any"});
            this.CellColorComboBox.Location = new System.Drawing.Point(114, 161);
            this.CellColorComboBox.Name = "CellColorComboBox";
            this.CellColorComboBox.Size = new System.Drawing.Size(121, 21);
            this.CellColorComboBox.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label8.Location = new System.Drawing.Point(4, 137);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Calc Quality";
            // 
            // QualityComboBox
            // 
            this.QualityComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "No",
            "Yes"});
            this.QualityComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.QualityComboBox.FormattingEnabled = true;
            this.QualityComboBox.Items.AddRange(new object[] {
            "No",
            "Yes"});
            this.QualityComboBox.Location = new System.Drawing.Point(159, 133);
            this.QualityComboBox.Name = "QualityComboBox";
            this.QualityComboBox.Size = new System.Drawing.Size(76, 21);
            this.QualityComboBox.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label7.Location = new System.Drawing.Point(6, 109);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Label Mode";
            // 
            // LabelComboBox
            // 
            this.LabelComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "Standard",
            "Dot peen",
            "Fax"});
            this.LabelComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelComboBox.FormattingEnabled = true;
            this.LabelComboBox.Items.AddRange(new object[] {
            "Standard",
            "Dot peen",
            "Fax",
            "St+Dot"});
            this.LabelComboBox.Location = new System.Drawing.Point(114, 105);
            this.LabelComboBox.Name = "LabelComboBox";
            this.LabelComboBox.Size = new System.Drawing.Size(121, 21);
            this.LabelComboBox.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label6.Location = new System.Drawing.Point(6, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Decode Mode";
            // 
            // SpeedComboBox
            // 
            this.SpeedComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "Ultimate",
            "Regular",
            "Express",
            "Fast"});
            this.SpeedComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SpeedComboBox.FormattingEnabled = true;
            this.SpeedComboBox.Items.AddRange(new object[] {
            "Ultimate",
            "Regular",
            "Express",
            "Fast"});
            this.SpeedComboBox.Location = new System.Drawing.Point(114, 78);
            this.SpeedComboBox.Name = "SpeedComboBox";
            this.SpeedComboBox.Size = new System.Drawing.Size(121, 21);
            this.SpeedComboBox.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label5.Location = new System.Drawing.Point(6, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Mirror";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label4.Location = new System.Drawing.Point(6, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "max DM Count";
            // 
            // MaxDMCountEdit
            // 
            this.MaxDMCountEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaxDMCountEdit.Location = new System.Drawing.Point(173, 25);
            this.MaxDMCountEdit.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.MaxDMCountEdit.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MaxDMCountEdit.Name = "MaxDMCountEdit";
            this.MaxDMCountEdit.Size = new System.Drawing.Size(62, 20);
            this.MaxDMCountEdit.TabIndex = 1;
            this.MaxDMCountEdit.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // MirrorComboBox
            // 
            this.MirrorComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "Normal",
            "Mirror",
            "Any"});
            this.MirrorComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MirrorComboBox.FormattingEnabled = true;
            this.MirrorComboBox.Items.AddRange(new object[] {
            "Normal",
            "Mirror",
            "Normal & Mirror"});
            this.MirrorComboBox.Location = new System.Drawing.Point(114, 51);
            this.MirrorComboBox.Name = "MirrorComboBox";
            this.MirrorComboBox.Size = new System.Drawing.Size(121, 21);
            this.MirrorComboBox.TabIndex = 0;
            // 
            // CodeDecoderOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 319);
            this.Controls.Add(this.OptionsGroupBox);
            this.Name = "CodeDecoderOptions";
            this.Text = "CodeDecoderOptions";
            this.OptionsGroupBox.ResumeLayout(false);
            this.OptionsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimeOutEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxDMCountEdit)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox OptionsGroupBox;
        private System.Windows.Forms.NumericUpDown TimeOutEdit;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox QzComboBox;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox FilterComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox CellColorComboBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox QualityComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox LabelComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox SpeedComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown MaxDMCountEdit;
        private System.Windows.Forms.ComboBox MirrorComboBox;
    }
}