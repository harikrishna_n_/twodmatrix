using System;
using System.Collections.Generic;
using System.Windows.Forms;
using PylonC.NET;

namespace Saatvik.TwoDMatrix
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            /* This is a special debug setting needed only for GigE cameras.
                See 'Building Applications with pylon' in the Programmer's Guide. */
            Environment.SetEnvironmentVariable("PYLON_GIGE_HEARTBEAT", "3000" /*ms*/);

            try
            {
                Pylon.Initialize();
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                Pylon.Terminate();
            }
            finally
            {
                Pylon.Terminate();
            }
        }
    }
}