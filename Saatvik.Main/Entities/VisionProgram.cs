﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saatvik.Entities
{
    public class VisionProgram
    {
        public string Name { get; set; }
        public int Rows { get; set; }
        public int Columns { get; set; }
        public System.Drawing.Rectangle Region { get; set; }
        public int RotationAngle { get; set; }
        public decimal MaxDMCount { get; set; }
        public int Mirror { get; set; }
        public int DecodeMode { get; set; }
        public int LabelMode { get; set; }
        public int CalcQuality { get; set; }
        public int Color { get; set; }
        public int Filter { get; set; }
        public int Quietzone { get; set; }
        public decimal Timeout { get; set; }
    }
}
