﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Saatvik.Entities
{
    public static class AppData
    {
        private static string DataFilePath = AppDomain.CurrentDomain.BaseDirectory + "Saatvik.Data.dll";
        public static void SaveDataPath(string val)
        {

            if (!File.Exists(DataFilePath))
            {
                var fs = File.Create(DataFilePath);
                fs.Close();
            }

            var fileStream = File.Create(DataFilePath);
            fileStream.Close();

            using (StreamWriter sw = new StreamWriter(DataFilePath))
            {
                sw.WriteLine(val);
            }
        }

        public static string ReadDataPath()
        {
            var path = string.Empty;
            if (File.Exists(DataFilePath))
            {
                using (StreamReader sr = new StreamReader(DataFilePath))
                {
                    path = sr.ReadLine();
                }
            }
            return path;
        }


        public static Boolean IsFolderPathConfigured()
        {
            var path = string.Empty;

            if (File.Exists(DataFilePath))
            {
                using (StreamReader sr = new StreamReader(DataFilePath))
                {
                    path = sr.ReadLine();
                    if (Directory.Exists(path))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            else return false;
        }
    }   
}
